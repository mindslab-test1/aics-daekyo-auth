FROM openjdk:8-jre-alpine
MAINTAINER junsu.jang@mindslab.ai

VOLUME /tmp

ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app

ENTRYPOINT ["java","-cp","app:app/lib/*","ai.mindslab.daekyo.auth.DaekyoAuthApplication"]
