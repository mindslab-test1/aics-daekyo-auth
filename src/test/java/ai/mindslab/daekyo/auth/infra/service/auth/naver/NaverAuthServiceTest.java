package ai.mindslab.daekyo.auth.infra.service.auth.naver;

import ai.mindslab.daekyo.auth.infra.config.HttpConnectionConfig;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = HttpConnectionConfig.class)
public class NaverAuthServiceTest {
    private static final String IDLE_ACCESS_TOKEN = "AAAAOSJ7874gRuXfz04FpivhzJb+LY9CiOeZQKKFju1oMM086WXJDPrSGix6nHuXo6/mchcdP/DdlCIHCTZJ2pxg64U=";
    private static final String EMPTY_ACCESS_TOKEN = "";

    @Autowired
    private RestTemplate restTemplate;

    private NaverAuthService naverAuthService;

    @BeforeEach
    public void setup() {
        naverAuthService = new NaverAuthService(restTemplate);
    }

    @Disabled
    @DisplayName("네이버의 Access Token을 검증하는 테스트")
    @Test
    public void test_naver_auth_service() {
        // given, when
        Pair<String, String> pair = naverAuthService.authorize(IDLE_ACCESS_TOKEN);
        String socialId = pair.getLeft();
        String email = pair.getRight();

        // then
        assertThat(socialId).isNotBlank();
        System.out.println("socialId: " + socialId);
        assertThat(email).isNotBlank();
        System.out.println("email: " + email);
    }

    @DisplayName("네이버 Token 인증이 실패할 경우의 테스트")
    @Test
    public void test_naver_token_verify_failed() {
        // given, when
        assertThatThrownBy(() -> naverAuthService.authorize(EMPTY_ACCESS_TOKEN))
                .isInstanceOf(InvalidTokenException.class)
                .hasMessageContaining("Failed naver authentication about access_token. please check expired or invalid token.");
    }
}
