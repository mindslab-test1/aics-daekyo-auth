package ai.mindslab.daekyo.auth.infra.service.auth.facebook;

import ai.mindslab.daekyo.auth.infra.config.HttpConnectionConfig;
import ai.mindslab.daekyo.auth.infra.config.property.social.FacebookProperty;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {HttpConnectionConfig.class, FacebookProperty.class})
public class FacebookAuthServiceTest {
    private static final String IDLE_ACCESS_TOKEN = "EAACHBsW8upEBAJ1WEP7bGQR1vnXjLFXleWJYHAyh1HUXrOZAZAHk1HBGZA2LoZCSj4FxJQY1JpbpG6fY1ZBwHkMYP88qGNqeA0ZCbZB1gfA8UZBJzqA9Y6ghqVaFrbUWjs31eIjjzZAN3bll7WkNVslOYZAO6EH8h5aivLFrq0w9PQJC0QyNqZBMZCS63hOdDZCmuPs2iCKFYqhdFRxqhDraVk3iDZARLEwz5U4ZClAgbhWn1EGh4pk8JKldh5Q";
    private static final String EMPTY_ACCESS_TOKEN = "";

    private static final String FACEBOOK_CLIENT_ID = "148463157033617";
    private static final String FACEBOOK_CLIENT_SECRET = "0b02352008b2f3a3092c0179c1a0eee8";

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private FacebookProperty mockFacebookProperty;

    private FacebookAuthService facebookAuthService;


    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(mockFacebookProperty, "clientId", FACEBOOK_CLIENT_ID);
        ReflectionTestUtils.setField(mockFacebookProperty, "clientSecret", FACEBOOK_CLIENT_SECRET);
        FacebookGraphAPI facebookGraphAPI = new FacebookGraphAPI(restTemplate, mockFacebookProperty);
        facebookAuthService = new FacebookAuthService(facebookGraphAPI);
    }

    @DisplayName("페이스북 ClientId/ClientSecret Test Injection 테스트")
    @Test
    public void test_mock_social_client() {
        assertThat(mockFacebookProperty.getClientId()).isEqualTo(FACEBOOK_CLIENT_ID);
        System.out.println("facebook.client-id: " + mockFacebookProperty.getClientId());
        assertThat(mockFacebookProperty.getClientSecret()).isEqualTo(FACEBOOK_CLIENT_SECRET);
        System.out.println("facebook.client-secret: " + mockFacebookProperty.getClientSecret());
    }

    @Disabled
    @DisplayName("페이스북의 Access Token을 검증하는 테스트")
    @Test
    public void test_facebook_auth_service() {
        // given, when
        Pair<String, String> pair = facebookAuthService.authorize(IDLE_ACCESS_TOKEN);
        String socialId = pair.getLeft();
        String email = pair.getRight();

        // then
        assertThat(socialId).isNotBlank();
        System.out.println("socialId: " + socialId);
        assertThat(email).isNotBlank();
        System.out.println("email: " + email);
    }

    @DisplayName("페이스북 Token 인증이 실패할 경우의 테스트")
    @Test
    public void test_facebook_token_verify_failed() {
        // given, when
        assertThatThrownBy(() -> facebookAuthService.authorize(EMPTY_ACCESS_TOKEN)).isInstanceOf(InvalidTokenException.class)
                .hasMessageContaining("Failed facebook authentication about access_token. please check expired or invalid token.");
    }
}
