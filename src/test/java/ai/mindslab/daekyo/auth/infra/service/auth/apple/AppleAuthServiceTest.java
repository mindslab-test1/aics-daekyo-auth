package ai.mindslab.daekyo.auth.infra.service.auth.apple;

import ai.mindslab.daekyo.auth.infra.config.HttpConnectionConfig;
import ai.mindslab.daekyo.auth.infra.config.property.social.AppleProperty;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {HttpConnectionConfig.class, AppleProperty.class})
class AppleAuthServiceTest {
    private static final String APPLE_CLIENT_ID = "ios.ai.maum.fanmeet";
    private static final String APPLE_KEY_ID = "V77W54M5G2";
    private static final String APPLE_TEAM_ID = "7A6BG8T7P2";
    private static final String APPLE_ISSUE_URL = "https://appleid.apple.com";
    private static final String APPLE_TOKEN_URL = "https://appleid.apple.com/auth/token";
    private static final String APPLE_PUBLIC_KEY_URL = "https://appleid.apple.com/auth/keys";
    private static final String APPLE_PRIVATE_KEY = "-----BEGIN PRIVATE KEY-----\n" +
            "MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgSCd/rVS0oSfTJXK1\n" +
            "HtlfONNmMJiGskKOZHhe/xS41kigCgYIKoZIzj0DAQehRANCAASFe6rbUa/yGIZg\n" +
            "ycRF4oSuXaWyFsvBnsvUNlNkFog2z54oLS4tmE4bDUsq8wy7QJuwgZxidFdNh2O2\n" +
            "cb6msSuu\n" +
            "-----END PRIVATE KEY-----";

    private static final String IDLE_ID_TOKEN = "eyJraWQiOiJlWGF1bm1MIiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoiaW9zLmFpLm1hdW0uZmFubWVldCIsImV4cCI6MTYwOTkyNzY0MSwiaWF0IjoxNjA5ODQxMjQxLCJzdWIiOiIwMDA4MzAuZjIyOGU3NDY3Mzg0NGNkOGI2MTdhYjUwNGVkNWE0MjAuMDk1NSIsImNfaGFzaCI6Ing5Q0pmdmNIX0ZhMHhSc3lfZHNMU0EiLCJlbWFpbCI6InNlb2toeWVvbjEyMzQ1QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjoidHJ1ZSIsImF1dGhfdGltZSI6MTYwOTg0MTI0MSwibm9uY2Vfc3VwcG9ydGVkIjp0cnVlfQ.BglKjXf_dLQShz4hRaSGxKxnFXj4vxuhyL-MNK9jQLuBSDWFCE5C_ZUdQNKPemrGgr07B1SiiBh6aNxnMU9PhkFVaD57v08fsqdZd4wV6kpRTXmAIzVakFzX1l2AazUsydCyOE31vY8llVVReE_06YUWW_1OqvLWi_7hnCl1th_nbOdq1k80IO9CInm6r9ovRfWmVvP3dqUvP-muPnf38eBsl4JFZUFx_3mMeEmsJZLEu9WkEmZ6c7LtU7A9wTb5r8pILYOr7FZrsIK8XE_TOa6x6wClQe5K7hqoClSi--D9AUveN_gxsOQYxGO9yx1ie8RtgItS_XKf_qrUJCkxAg";
    private static final String EMPTY_ID_TOKEN = "";

    private static final String IDLE_AUTH_CODE = "cf30c87e4eaa84f59a77b95f03c2f5e49.0.nytq.ncRrXko_s8NF86kIpvjO9w";
    private static final String EMPTY_AUTH_CODE = "";

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private AppleProperty mockAppleProperty;

    private AppleAuthService appleAuthService;

    private void initMockAppleProperty() {
        ReflectionTestUtils.setField(mockAppleProperty, "clientId", APPLE_CLIENT_ID);
        ReflectionTestUtils.setField(mockAppleProperty, "issueUrl", APPLE_ISSUE_URL);
        ReflectionTestUtils.setField(mockAppleProperty, "teamId", APPLE_TEAM_ID);
        ReflectionTestUtils.setField(mockAppleProperty, "keyId", APPLE_KEY_ID);
        ReflectionTestUtils.setField(mockAppleProperty, "authTokenUrl", APPLE_TOKEN_URL);
        ReflectionTestUtils.setField(mockAppleProperty, "publicKeyUrl", APPLE_PUBLIC_KEY_URL);
        ReflectionTestUtils.setField(mockAppleProperty, "privateKey", APPLE_PRIVATE_KEY);
    }

    @BeforeEach
    public void setup() {
        initMockAppleProperty();
        AppleAuthAPI appleAuthAPI = new AppleAuthAPI(restTemplate, mockAppleProperty);
        appleAuthService = new AppleAuthService(appleAuthAPI, mockAppleProperty);
    }

    @DisplayName("애플 ClientId/ClientSecret Test Injection 테스트")
    @Test
    public void test_apple_client() {
        assertThat(mockAppleProperty.getClientId()).isEqualTo(APPLE_CLIENT_ID);
        System.out.println("apple.client-id: " + mockAppleProperty.getClientId());

        assertThat(mockAppleProperty.getKeyId()).isEqualTo(APPLE_KEY_ID);
        System.out.println("apple.key-id: " + mockAppleProperty.getKeyId());

        assertThat(mockAppleProperty.getTeamId()).isEqualTo(APPLE_TEAM_ID);
        System.out.println("apple.team-id: " + mockAppleProperty.getTeamId());

        assertThat(mockAppleProperty.getIssueUrl()).isEqualTo(APPLE_ISSUE_URL);
        System.out.println("apple.issue-url: " + mockAppleProperty.getIssueUrl());

        assertThat(mockAppleProperty.getAuthTokenUrl()).isEqualTo(APPLE_TOKEN_URL);
        System.out.println("apple.token-url: " + mockAppleProperty.getAuthTokenUrl());

        assertThat(mockAppleProperty.getPublicKeyUrl()).isEqualTo(APPLE_PUBLIC_KEY_URL);
        System.out.println("apple.public-key-url: " + mockAppleProperty.getPublicKeyUrl());

        assertThat(mockAppleProperty.getPrivateKey()).isEqualTo(APPLE_PRIVATE_KEY);
        System.out.println("apple.private-key: " + mockAppleProperty.getPrivateKey());
    }

    @Disabled
    @DisplayName("애플의 IdToken, AuthorizationCode를 검증하는 테스트")
    @Test
    public void test_apple_auth_service() {
        // given, when
        Pair<String, String> pair = appleAuthService.authorize(IDLE_ID_TOKEN, IDLE_AUTH_CODE, true);
        String socialId = pair.getLeft();
        String email = pair.getRight();

        // then
        assertThat(socialId).isNotBlank();
        System.out.println("socialId: " + socialId);
        assertThat(email).isNotBlank();
        System.out.println("email: " + email);
    }

    @DisplayName("애플의 IdToken 인증이 실패할 경우의 테스트 - Empty")
    @Test
    public void test_apple_id_token_verify_failed_empty() {
        // given, when
        assertThatThrownBy(() -> appleAuthService.authorize(EMPTY_ID_TOKEN, IDLE_AUTH_CODE, true)).isInstanceOf(InvalidTokenException.class);
    }

    @DisplayName("애플의 IdToken 인증이 실패할 경우의 테스트")
    @Test
    public void test_apple_id_token_verify_failed() {
        // given
        final String TEMP_ID_TOKEN = "eyJraWQiOiJVV01VSFNaSzcyIiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiIzTUVUNjZVTUVCIiwiYXVkIjoiaHR0cHM6Ly9hcHBsZWlkLmFwcGxlLmNvbSIsInN1YiI6ImNvbS5ka2Jvb2t0YWxrLmRrYm9va3RhbGtBcHAiLCJleHAiOjE2MDg3OTk1ODgsImlhdCI6MTYwODc5OTI4OH0.94eqQst5Mdde62qdn2UOSNZ7pP1JOTxsxCbJqERifxAty7G7HrxtlPDf9zwllKF5cbIoZ-Y4srkZT7sJXuQhzg";
        // when, then
        assertThatThrownBy(() -> appleAuthService.authorize(TEMP_ID_TOKEN, IDLE_AUTH_CODE, true)).isInstanceOf(InvalidTokenException.class)
                .hasMessageContaining("Failed apple authentication about idToken. please check invalid token.");
    }

    @DisplayName("요청한 IdToken과 Apple의 IdToken이 불일치할 경우의 테스트 - Empty")
    @Test
    public void test_do_not_match_apple_id_token_empty() {
        // given, when
        assertThatThrownBy(() -> appleAuthService.authorize(IDLE_ID_TOKEN, EMPTY_AUTH_CODE, true)).isInstanceOf(InvalidTokenException.class);
    }

    @Disabled
    @DisplayName("요청한 IdToken과 Apple의 IdToken이 불일치할 경우의 테스트")
    @Test
    public void test_do_not_match_apple_id_token() {
        // given
        final String TEMP_AUTH_CODE = "41235qwegasdf12";
        // when, then
        assertThatThrownBy(() -> appleAuthService.authorize(IDLE_ID_TOKEN, TEMP_AUTH_CODE, true)).isInstanceOf(InvalidTokenException.class)
                .hasMessageContaining("Failed apple authentication about idToken. the requested idToken and apple's idToken do not match.");
    }

    @DisplayName("애플의 IdToken 검증을 무시하는 경우의 테스트")
    @Test
    public void test_do_not_verify_apple_id_token() {
        // given
        final String TEMP_AUTH_CODE = "asdf";
        // when, then
        Pair<String, String> pair = appleAuthService.authorize(IDLE_ID_TOKEN, TEMP_AUTH_CODE, false);
        String socialId = pair.getLeft();
        String email = pair.getRight();

        // then
        assertThat(socialId).isNotBlank();
        System.out.println("socialId: " + socialId);
        assertThat(email).isNotBlank();
        System.out.println("email: " + email);
    }
}
