package ai.mindslab.daekyo.auth.infra.service.auth.apple;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
public class AppleIdTokenTest {

    private static final String IDLE_ID_TOKEN = "eyJraWQiOiJlWGF1bm1MIiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJodHRwczovL2FwcGxlaWQuYXBwbGUuY29tIiwiYXVkIjoiaW9zLmFpLm1hdW0uZmFubWVldCIsImV4cCI6MTYwOTkyNzY0MSwiaWF0IjoxNjA5ODQxMjQxLCJzdWIiOiIwMDA4MzAuZjIyOGU3NDY3Mzg0NGNkOGI2MTdhYjUwNGVkNWE0MjAuMDk1NSIsImNfaGFzaCI6Ing5Q0pmdmNIX0ZhMHhSc3lfZHNMU0EiLCJlbWFpbCI6InNlb2toeWVvbjEyMzQ1QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjoidHJ1ZSIsImF1dGhfdGltZSI6MTYwOTg0MTI0MSwibm9uY2Vfc3VwcG9ydGVkIjp0cnVlfQ.BglKjXf_dLQShz4hRaSGxKxnFXj4vxuhyL-MNK9jQLuBSDWFCE5C_ZUdQNKPemrGgr07B1SiiBh6aNxnMU9PhkFVaD57v08fsqdZd4wV6kpRTXmAIzVakFzX1l2AazUsydCyOE31vY8llVVReE_06YUWW_1OqvLWi_7hnCl1th_nbOdq1k80IO9CInm6r9ovRfWmVvP3dqUvP-muPnf38eBsl4JFZUFx_3mMeEmsJZLEu9WkEmZ6c7LtU7A9wTb5r8pILYOr7FZrsIK8XE_TOa6x6wClQe5K7hqoClSi--D9AUveN_gxsOQYxGO9yx1ie8RtgItS_XKf_qrUJCkxAg";

    @DisplayName("애플 IdToken decoeded 테스트")
    @Test
    public void test() {
        ApplePayload applePayload;
        try {
            applePayload = AppleUtils.decodeFromIdToken(IDLE_ID_TOKEN);
        } catch (ParseException | JsonProcessingException e) {
            throw new InvalidTokenException(e.getLocalizedMessage());
        }

        String email = applePayload.getEmail();
        System.out.println(email);
        assertThat(email).isEqualTo("seokhyeon12345@gmail.com");

        String clientId = applePayload.getSub();
        System.out.println(clientId);
        assertThat(clientId).isEqualTo("000830.f228e74673844cd8b617ab504ed5a420.0955");
    }
}
