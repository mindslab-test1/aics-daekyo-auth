package ai.mindslab.daekyo.auth.infra.service.auth.google;

import ai.mindslab.daekyo.auth.infra.config.HttpConnectionConfig;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = HttpConnectionConfig.class)
class GoogleAuthServiceTest {
    private static final String IDLE_ACCESS_TOKEN = "ya29.A0AfH6SMCjZit-KHlIkYw4Ohk33O_TAP4aZEXHJqKWPRdf4futw7yKR7-bNu-bIhAhHpyZmTt4z0O5q5og8ZDn81IXdz1DAsuWgiRoH6PvLVlIA6yg93x1zHytgm2O61-msoHAqhwuzdpu6DkiIM5d6uP4JPPt4Ed-wqQAgd0tAtM";
    private static final String EMPTY_ACCESS_TOKEN = "";

    @Autowired
    private RestTemplate restTemplate;

    private GoogleAuthService googleAuthService;

    @BeforeEach
    public void setup() {
        googleAuthService = new GoogleAuthService(restTemplate);
    }

    @Disabled
    @DisplayName("구글의 Access Token을 검증하는 테스트")
    @Test
    public void test_google_auth_service() {
        // given, when
        Pair<String, String> pair = googleAuthService.authorize(IDLE_ACCESS_TOKEN);
        String socialId = pair.getLeft();
        String email = pair.getRight();

        // then
        assertThat(socialId).isNotBlank();
        System.out.println("socialId: " + socialId);
        assertThat(email).isNotBlank();
        System.out.println("email: " + email);
    }

    @DisplayName("구글 Token 인증이 실패할 경우의 테스트")
    @Test
    public void test_google_token_verify_failed() {
        // given, when
        assertThatThrownBy(() -> {
            googleAuthService.authorize(EMPTY_ACCESS_TOKEN);
        }).isInstanceOf(InvalidTokenException.class)
        .hasMessageContaining("Failed google authentication about access_token. please check expired or invalid token.");
    }

}
