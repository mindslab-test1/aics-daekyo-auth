package ai.mindslab.daekyo.auth.core.usecase;

import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.core.repository.UserRepository;
import ai.mindslab.daekyo.auth.core.usecase.user.RegisterUserUsecase;
import test.integration.ExternalAPITest;
import ai.mindslab.daekyo.auth.infra.config.property.DaekyoProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = {DaekyoProperty.class})
class RegisterUserUsecaseTest extends ExternalAPITest {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private DaekyoProperty daekyoProperty;

    @MockBean
    private UserRepository mockUserRepository;

    private RegisterUserUsecase registerUserUsecase;

    @BeforeEach
    public void setup() {
        registerUserUsecase = new RegisterUserUsecase(mockUserRepository, restTemplate, daekyoProperty);
    }

    @Disabled
    @DisplayName("mock-bean di test")
    @Test
    public void test_mock_bean() {
        assertThat(mockUserRepository).isNotNull();
    }

    @Disabled
    @DisplayName("User 등록 테스트 - socialLogin response data format test")
    @Test
    public void test_register_user() {
        final String name = "장준수";
        final String email = "melon4758@gmail.com";
        final String uniqueId = "12345678";

        UserDetails userDetails = registerUserUsecase.execute(DaekyoOAuth2Type.GOOGLE, uniqueId, email, name);
        System.out.println(userDetails.toString());
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(name);
    }
}
