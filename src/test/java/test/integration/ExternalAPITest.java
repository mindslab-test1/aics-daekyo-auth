package test.integration;

import ai.mindslab.daekyo.auth.infra.config.HttpConnectionConfig;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = {HttpConnectionConfig.class})
public abstract class ExternalAPITest implements IntegrationTest {
}
