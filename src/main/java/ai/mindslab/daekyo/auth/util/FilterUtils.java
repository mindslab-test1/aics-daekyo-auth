package ai.mindslab.daekyo.auth.util;

import org.apache.catalina.connector.RequestFacade;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public class FilterUtils {

    public static HttpServletRequest checkHttpRequestObject(ServletRequest servletRequest) {
        if (servletRequest instanceof RequestFacade) {
            return (RequestFacade) servletRequest;
        } else {
            return (HttpServletRequest) servletRequest;
        }
    }
}
