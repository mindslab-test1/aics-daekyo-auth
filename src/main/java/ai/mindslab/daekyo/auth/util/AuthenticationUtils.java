package ai.mindslab.daekyo.auth.util;

import org.springframework.security.core.Authentication;

import java.util.Collections;
import java.util.Map;

public class AuthenticationUtils {

    @SuppressWarnings("unchecked") // Remove down casting warning
    public static Map<String, Object> getDetailsMap(Authentication authentication) {
        Object details = authentication.getDetails();
        if (!(details instanceof Map)) {
            return Collections.emptyMap();
        }
        return (Map<String, Object>) details;
    }
}
