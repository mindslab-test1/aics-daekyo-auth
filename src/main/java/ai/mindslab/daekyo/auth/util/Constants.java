package ai.mindslab.daekyo.auth.util;

public class Constants {
    // rest api header
    public static final String APP_KEY_HEADER = "APP-KEY";

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String NOOP = "{noop}";

    public static final String IS_VERIFY = "is_verify";
    public static final String CODE = "code";
    public static final String ID_TOKEN = "id_token";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String INPUT_TOKEN = "input_token";
    public static final String FIELDS = "fields";

    public static final String GRANT_TYPE = "grant_type";
    public static final String COMPANY_NO = "company_no";
    public static final String SOCIAL_ID = "social_id";
    public static final String EMAIL = "email";
    public static final String NAME = "name";

    public static final String NEWLINE = "\n";

    private Constants() { }
}
