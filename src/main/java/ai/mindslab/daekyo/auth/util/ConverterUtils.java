package ai.mindslab.daekyo.auth.util;

import org.apache.logging.log4j.util.Strings;

public class ConverterUtils {

    public static String convertToString(Boolean attribute) {
        if (attribute != null) {
            return (attribute) ? "Y" : "N";
        }
        return "N";
    }

    public static Boolean convertToBoolean(String attribute) {
        if (!Strings.isBlank(attribute)) {
            return attribute.equalsIgnoreCase("Y");
        }
        return false;
    }
}
