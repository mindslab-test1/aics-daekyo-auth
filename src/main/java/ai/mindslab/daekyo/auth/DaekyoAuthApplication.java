package ai.mindslab.daekyo.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DaekyoAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(DaekyoAuthApplication.class, args);
    }

}
