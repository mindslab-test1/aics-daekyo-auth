package ai.mindslab.daekyo.auth.boundaries.controller;

import ai.mindslab.daekyo.auth.boundaries.dto.ClientTokenUpdateDto;
import ai.mindslab.daekyo.auth.boundaries.dto.ClientUpdateDto;
import ai.mindslab.daekyo.auth.core.model.Client;
import ai.mindslab.daekyo.auth.core.repository.ClientRepository;
import ai.mindslab.daekyo.auth.core.usecase.client.ClientDetailUsecase;
import ai.mindslab.daekyo.auth.core.usecase.client.UpdateClientUsecase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class ClientController {
    private final ClientRepository clientRepository;
    private final ClientDetailUsecase clientDetailUsecase;
    private final UpdateClientUsecase updateClientUsecase;

    @GetMapping("/clients")
    public ResponseEntity<Page<Client>> list(
            @PageableDefault(size = 20) Pageable pageable
    ) {
        Page<Client> clientPage = clientRepository.findAll(PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.Direction.ASC, "id"));
        return new ResponseEntity<>(clientPage, HttpStatus.OK);
    }

    @GetMapping("/client/{id}")
    public ResponseEntity<Client> detail(@PathVariable Long id) {
        return new ResponseEntity<>(clientDetailUsecase.execute(id), HttpStatus.OK);
    }

    @PutMapping("/client/{id}")
    public ResponseEntity<Client> update(
            @PathVariable Long id,
            @RequestBody @Valid ClientUpdateDto clientUpdateDto,
            BindingResult bindingResult
    ) throws BindException {
        if (bindingResult.hasErrors()) throw new BindException(bindingResult);
        return new ResponseEntity<>(updateClientUsecase.execute(id, clientUpdateDto), HttpStatus.RESET_CONTENT);
    }

    @PatchMapping("/client/{id}")
    public ResponseEntity<Client> update(
            @PathVariable Long id,
            @RequestBody @Valid ClientTokenUpdateDto clientTokenUpdateDto,
            BindingResult bindingResult
    ) throws BindException {
        if (bindingResult.hasErrors()) throw new BindException(bindingResult);
        return new ResponseEntity<>(updateClientUsecase.execute(id, clientTokenUpdateDto), HttpStatus.RESET_CONTENT);
    }
}
