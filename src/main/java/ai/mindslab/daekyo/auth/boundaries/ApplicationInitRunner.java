package ai.mindslab.daekyo.auth.boundaries;

import ai.mindslab.daekyo.auth.core.model.Client;
import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.core.repository.ClientRepository;
import ai.mindslab.daekyo.auth.util.Constants;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Profile("local")
@Component
public class ApplicationInitRunner implements ApplicationRunner {
    private final ClientRepository clientRepository;
    private final PasswordEncoder bCryptPasswordEncoder;

    private static final Logger logger = LoggerFactory.getLogger(ApplicationInitRunner.class);

    @Override
    public void run(ApplicationArguments args) throws Exception {
        final String REFRESH_TOKEN = "refresh_token";
        Set<String> defaultScopes = new HashSet<>();
        defaultScopes.add("read");
        defaultScopes.add("write");
        defaultScopes.add("trust");

        final String clientIdAccount = "daekyo-account";
        Optional<Client> optionalClientAccount = clientRepository.findByClientId(clientIdAccount);
        if (!optionalClientAccount.isPresent()) {
            Set<String> authorizedGrantTypes = Arrays.stream(DaekyoOAuth2Type.values()).filter(daekyoOAuth2Type -> !daekyoOAuth2Type.equals(DaekyoOAuth2Type.PASSWORD)).map(daekyoOAuth2Type -> daekyoOAuth2Type.name().toLowerCase()).collect(Collectors.toSet());
            authorizedGrantTypes.add(REFRESH_TOKEN);
            final String authorityAccount = "DAEKYO_ACCOUNT";
            Set<String> authorities = new HashSet<>();
            authorities.add(authorityAccount);

            Client client = clientRepository.save(Client.builder()
                    .id(null)
                    .clientId(clientIdAccount)
                    .clientSecret(Constants.NOOP + "daekyo-account-secret12345")
                    .resourceIds(Collections.emptySet())
                    .scopes(defaultScopes)
                    .authorizedGrantTypes(authorizedGrantTypes)
                    .webServerRedirectUris(Collections.emptySet())
                    .authorities(authorities)
                    .accessTokenValiditySeconds(60 * 60 * 2)
                    .refreshTokenValiditySeconds(60 * 60 * 60)
                    .additionalInformation(Collections.emptyMap())
                    .autoApprove(true)
                    .createdAt(null)
                    .updatedAt(null)
                    .build());
            logger.info("new {}", client);
        } else {
            logger.info("already exist {}", optionalClientAccount.get());
            Client client = optionalClientAccount.get();
            client.setClientSecret(bCryptPasswordEncoder.encode("daekyo-account-secret12345"));
            logger.info(clientRepository.save(client).toString());
        }

        final String clientIdTeacher = "daekyo-teacher";
        Optional<Client> optionalClientTeacher = clientRepository.findByClientId(clientIdTeacher);
        if (!optionalClientTeacher.isPresent()) {
            Set<String> authorizedGrantTypes = new HashSet<>();
            authorizedGrantTypes.add(DaekyoOAuth2Type.PASSWORD.name().toLowerCase());
            authorizedGrantTypes.add(REFRESH_TOKEN);
            final String authorityAccount = "DAEKYO_UDONG_TEACHER";
            Set<String> authorities = new HashSet<>();
            authorities.add(authorityAccount);

            Client client = clientRepository.save(Client.builder()
                    .id(null)
                    .clientId(clientIdTeacher)
                    .clientSecret(Constants.NOOP + "daekyo-teacher-secret12345")
                    .resourceIds(Collections.emptySet())
                    .scopes(defaultScopes)
                    .authorizedGrantTypes(authorizedGrantTypes)
                    .webServerRedirectUris(Collections.emptySet())
                    .authorities(authorities)
                    .accessTokenValiditySeconds(60 * 60 * 2)
                    .refreshTokenValiditySeconds(60 * 60 * 60)
                    .additionalInformation(Collections.emptyMap())
                    .autoApprove(true)
                    .createdAt(null)
                    .updatedAt(null)
                    .build());
            logger.info("{}", client);
        } else {
            logger.info("already exist {}", optionalClientTeacher.get());
            Client client = optionalClientTeacher.get();
            client.setClientSecret(bCryptPasswordEncoder.encode("daekyo-teacher-secret12345"));
            logger.info(clientRepository.save(client).toString());
        }

        final String clientIdAdmin = "daekyo-admin";
        Optional<Client> optionalClientAdmin = clientRepository.findByClientId(clientIdAdmin);
        if (!optionalClientAdmin.isPresent()) {
            Set<String> authorizedGrantTypes = new HashSet<>();
            authorizedGrantTypes.add(DaekyoOAuth2Type.PASSWORD.name().toLowerCase());
            authorizedGrantTypes.add(REFRESH_TOKEN);
            final String authorityAccount = "DAEKYO_ADMIN";
            Set<String> authorities = new HashSet<>();
            authorities.add(authorityAccount);

            Client client = clientRepository.save(Client.builder()
                    .id(null)
                    .clientId(clientIdAdmin)
                    .clientSecret(Constants.NOOP + "daekyo-admin-secret12345")
                    .resourceIds(Collections.emptySet())
                    .scopes(defaultScopes)
                    .authorizedGrantTypes(authorizedGrantTypes)
                    .webServerRedirectUris(Collections.emptySet())
                    .authorities(authorities)
                    .accessTokenValiditySeconds(60 * 60 * 2)
                    .refreshTokenValiditySeconds(60 * 60 * 60)
                    .additionalInformation(Collections.emptyMap())
                    .autoApprove(true)
                    .createdAt(null)
                    .updatedAt(null)
                    .build());
            logger.info("{}", client);
        } else {
            logger.info("already exist {}", optionalClientAdmin.get());
            Client client = optionalClientAdmin.get();
            client.setClientSecret(bCryptPasswordEncoder.encode("daekyo-admin-secret12345"));
            logger.info(clientRepository.save(client).toString());
        }
    }
}
