package ai.mindslab.daekyo.auth.boundaries.dto;

import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class ClientTokenUpdateDto {
    @Positive
    private Integer accessTokenValiditySeconds;
    @Positive
    private Integer refreshTokenValiditySeconds;
}
