package ai.mindslab.daekyo.auth.boundaries;

import ai.mindslab.daekyo.auth.core.exception.NotFoundUserException;
import ai.mindslab.daekyo.auth.core.model.teacher.Teacher;
import ai.mindslab.daekyo.auth.core.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Profile("temp")
@Component
public class ApplicationTeacherRunner implements ApplicationRunner {
    private final TeacherRepository teacherRepository;
    private final PasswordEncoder passwordEncoder;

    private static final Logger logger = LoggerFactory.getLogger(ApplicationTeacherRunner.class);

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Teacher teacher = teacherRepository.findByCompanyNo(6L).orElseThrow(() -> new NotFoundUserException(""));
        teacher.setPasswordHashed(passwordEncoder.encode("1234"));

        logger.info(teacherRepository.save(teacher).toString());
    }
}
