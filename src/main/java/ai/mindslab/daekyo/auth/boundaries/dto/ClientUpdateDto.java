package ai.mindslab.daekyo.auth.boundaries.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;

@Data
public class ClientUpdateDto {
    @NotNull
    private String clientId;
    @NotNull
    private String clientSecret;
    private Collection<String> resourceIds;
    private Collection<String> scopes;
    @NotNull
    private Collection<String> authorizedGrantTypes;
    private Collection<String> webServerRedirectUris;
    @NotNull
    private Collection<String> authorities;
    @NotNull
    private Integer accessTokenValiditySeconds;
    @NotNull
    private Integer refreshTokenValiditySeconds;
    private Map<String, Object> additionalInformation;
    private boolean autoApprove;
}
