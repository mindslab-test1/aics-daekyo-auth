package ai.mindslab.daekyo.auth.boundaries;

import ai.mindslab.daekyo.auth.core.exception.BaseException;
import ai.mindslab.daekyo.auth.core.exception.DaekyoAuthApiResponseError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<Object> handleBaseException(BaseException exception, HttpStatus status, WebRequest request) {
        logger.debug("handleBaseException", exception);
        DaekyoAuthApiResponseError daekyoAuthApiResponseError = DaekyoAuthApiResponseError.builder()
                .code(exception.getCode())
                .message(exception.getMessage())
                .build();
        return handleExceptionInternal(exception, daekyoAuthApiResponseError, new HttpHeaders(), status, request);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.debug("handleBindException", ex);
        return handleExceptionInternal(ex, ex.getBindingResult(), headers, status, request);
    }
}
