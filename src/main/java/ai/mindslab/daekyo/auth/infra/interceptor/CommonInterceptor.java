package ai.mindslab.daekyo.auth.infra.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static ai.mindslab.daekyo.auth.util.Constants.NEWLINE;

@Component
public class CommonInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(CommonInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(NEWLINE);

        String url = request.getRequestURL().toString();

        String methodName = request.getMethod();

        String queryString = request.getQueryString();
        if (queryString != null) {
            url = url + "?" + queryString;
        }
        stringBuilder.append(String.format("[%s] %s", methodName, url)).append(NEWLINE);

        Map<String, String[]> params = request.getParameterMap();
        if (!params.isEmpty()) {
            for (String key : params.keySet()) {
                String[] value = params.get(key);
                stringBuilder.append(String.format("(p) <-- %s = %s", key, String.join(",", value))).append(NEWLINE);
            }
        }

        logger.info(stringBuilder.toString());

        return super.preHandle(request, response, handler);
    }
}
