package ai.mindslab.daekyo.auth.infra.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.lang.NonNull;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ai.mindslab.daekyo.auth.util.Constants.NEWLINE;

public class RestTemplateErrorHandler implements ResponseErrorHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return !response.getStatusCode().is2xxSuccessful();
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        final String error = getErrorAsString(response);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(NEWLINE);
        stringBuffer.append("==============================[Response]==============================").append(NEWLINE);
        stringBuffer.append("Headers : " + response.getHeaders()).append(NEWLINE);
        stringBuffer.append("Response Status : " + response.getRawStatusCode()).append(NEWLINE);
        stringBuffer.append("Request body : " + error).append(NEWLINE);
        logger.error(stringBuffer.toString());
    }

    private String getErrorAsString(@NonNull final ClientHttpResponse response) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(response.getBody()))) {
            StringBuffer stringBuffer = new StringBuffer();
            br.lines().forEach(stringBuffer::append);
            return stringBuffer.toString();
        }
    }
}
