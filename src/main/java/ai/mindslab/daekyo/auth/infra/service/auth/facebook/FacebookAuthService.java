package ai.mindslab.daekyo.auth.infra.service.auth.facebook;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class FacebookAuthService {
    private final FacebookGraphAPI facebookGraphAPI;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public Pair<String, String> authorize(String accessToken) {
        facebookGraphAPI.debugToken(accessToken);
        FacebookUserInfo facebookUserInfo = facebookGraphAPI.me(accessToken);
        return new ImmutablePair<>(facebookUserInfo.getId(), facebookUserInfo.getEmail());
    }
}
