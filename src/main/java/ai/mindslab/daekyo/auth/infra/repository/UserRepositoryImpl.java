package ai.mindslab.daekyo.auth.infra.repository;

import ai.mindslab.daekyo.auth.core.model.user.User;
import ai.mindslab.daekyo.auth.core.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RequiredArgsConstructor
@Repository
public class UserRepositoryImpl implements UserRepository {
    private final UserEntityRepository userEntityRepository;

    @Override
    public Optional<User> findBySocialAndEmail(String social, String email) {
        return userEntityRepository.findBySocialAndEmail(social, email).map(userEntity -> userEntity.toModel(userEntity));
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userEntityRepository.findByName(username).map(userEntity -> userEntity.toModel(userEntity));
    }
}
