package ai.mindslab.daekyo.auth.infra.config;

import ai.mindslab.daekyo.auth.infra.config.filter.CustomCorsFilter;
import ai.mindslab.daekyo.auth.infra.config.property.BasicAuthProperty;
import ai.mindslab.daekyo.auth.infra.custom.provider.DaekyoAuthenticationProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final CustomCorsFilter customCorsFilter;
    private final DaekyoAuthenticationProvider daekyoAuthenticationProvider;
    private final BasicAuthProperty basicAuthProperty;

    /**
     * Authentication Process를 관리하는 Manager Class
     * 인증 매커니즘의 핵심을 담은 클래스이다.
     * [Why Overriding]
     *  - AuthenticationManagerBuilder 를 통해 custom 할 예정이라면, 반드시 재정의후 bean으로 등록해야한다.
     * [Exception]
     *  - DisabledException: 서비스 이용이 제한된 User인 경우 (active == false)
     *  - LockedException: 보안 위협, 해킹 등 악의적으로 서비스에 영향을 준 경우 (locked == true)
     *  - BadCredentialsException: 아이디 or 비밀번호가 일치하지 않는 경우 (username/password incorrected)
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daekyoAuthenticationProvider);
    }

    /**
     * controller단까지 들어오기 이전까지 http call진행중 필요한 검증절차 및 인증을 설정해주는 클래스입니다.
     * Override하지 않는다면, http.authorizeRequests().anyRequest().authenticated().and().formLogin().and().httpBasic();
     * 다음과 같이 default로 적용되어지기 때문에 반드시 재정의가 필요한 method입니다.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .addFilterBefore(customCorsFilter, ChannelProcessingFilter.class)
                .authorizeRequests()
                    .antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
                    .antMatchers("/auth/**").permitAll()
                    .antMatchers("/daekyo-auth/profile").permitAll()
//                    .antMatchers("/auth/**").hasAnyAuthority(basicAuthProperty.getAdminAuthority())
//                    .antMatchers("/auth/**").hasAnyAuthority(DaekyoAuthority.DAEKYO_ADMIN.getAuthority())
                    .anyRequest().authenticated()
                .and()
                    .csrf().disable()
                    // form 기반의 로그인에 대해 비활성화 한다.
                    .formLogin().disable()
                    // 토큰을 활용하면 세션이 필요 없으므로 STATELESS로 설정하여 Session을 사용하지 않는다.
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        // prefix algorithm for DelegatingPasswordEncoder
        DelegatingPasswordEncoder delegatingPasswordEncoder = (DelegatingPasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder();
        delegatingPasswordEncoder.setDefaultPasswordEncoderForMatches(new BCryptPasswordEncoder());
        return delegatingPasswordEncoder;
    }
}
