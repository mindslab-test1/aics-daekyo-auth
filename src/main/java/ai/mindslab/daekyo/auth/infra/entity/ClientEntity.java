package ai.mindslab.daekyo.auth.infra.entity;

import ai.mindslab.daekyo.auth.core.model.Client;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Table(name = "DK_CLIENT")
public class ClientEntity extends BaseEntity {
    @Id
    @SequenceGenerator(name = "CLIENT_SEQ_GEN", sequenceName = "SEQ_CLIENT_ID", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLIENT_SEQ_GEN")
    private Long id;

    @Column(unique = true)
    private String clientId;

    @Column(unique = true)
    private String clientSecret;

    private String resourceIds;
    private String scopes;
    private String authorizedGrantTypes;
    private String webServerRedirectUris;
    private String authorities;
    private Integer accessTokenValiditySeconds = 60 * 60 * 2;
    private Integer refreshTokenValiditySeconds = 60 * 60 * 60 * 12;
    private String additionalInformation;
    private Boolean autoApprove;

    public Client toModel(ClientEntity clientEntity) {
        return Client.builder()
                .id(clientEntity.getId())
                .clientId(clientEntity.getClientId())
                .clientSecret(clientEntity.getClientSecret())
                .resourceIds((StringUtils.isEmpty(clientEntity.getResourceIds())) ? Collections.emptySet() : Arrays.stream(clientEntity.getResourceIds().split(",")).collect(Collectors.toSet()))
                .scopes((StringUtils.isEmpty(clientEntity.getScopes())) ? Collections.emptySet() : Arrays.stream(clientEntity.getScopes().split(",")).collect(Collectors.toSet()))
                .authorizedGrantTypes((StringUtils.isEmpty(clientEntity.getAuthorizedGrantTypes())) ? Collections.emptySet() : Arrays.stream(clientEntity.getAuthorizedGrantTypes().split(",")).collect(Collectors.toSet()))
                .webServerRedirectUris((StringUtils.isEmpty(clientEntity.getWebServerRedirectUris())) ? Collections.emptySet() : Arrays.stream(clientEntity.getWebServerRedirectUris().split(",")).collect(Collectors.toSet()))
                .authorities((StringUtils.isEmpty(clientEntity.getAuthorities())) ? Collections.emptySet() : Arrays.stream(clientEntity.getAuthorities().split(",")).collect(Collectors.toSet()))
                .accessTokenValiditySeconds(clientEntity.getAccessTokenValiditySeconds())
                .refreshTokenValiditySeconds(clientEntity.getRefreshTokenValiditySeconds())
                .additionalInformation(
                        (StringUtils.isEmpty(clientEntity.getAdditionalInformation())) ? Collections.emptyMap() :
                                Arrays.stream(clientEntity.additionalInformation.split(","))
                                        .map(entry -> entry.split("="))
                                        .collect(Collectors.toMap(entry -> entry[0], entry -> entry[1])))
                .autoApprove(clientEntity.getAutoApprove())
                .createdAt((clientEntity.getCreatedAt() == null) ? null : clientEntity.getCreatedAt().atZone(ZoneId.systemDefault()))
                .updatedAt((clientEntity.getUpdatedAt() == null) ? null : clientEntity.getUpdatedAt().atZone(ZoneId.systemDefault()))
                .build();
    }
}
