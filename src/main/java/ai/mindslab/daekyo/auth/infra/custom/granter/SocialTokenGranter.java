package ai.mindslab.daekyo.auth.infra.custom.granter;

import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.infra.service.auth.apple.AppleAuthService;
import ai.mindslab.daekyo.auth.infra.service.auth.apple.AppleAuthTokenGranter;
import ai.mindslab.daekyo.auth.infra.service.auth.facebook.FacebookAuthService;
import ai.mindslab.daekyo.auth.infra.service.auth.facebook.FacebookAuthTokenGranter;
import ai.mindslab.daekyo.auth.infra.service.auth.google.GoogleAuthService;
import ai.mindslab.daekyo.auth.infra.service.auth.google.GoogleAuthTokenGranter;
import ai.mindslab.daekyo.auth.infra.service.auth.naver.NaverAuthService;
import ai.mindslab.daekyo.auth.infra.service.auth.naver.NaverAuthTokenGranter;
import ai.mindslab.daekyo.auth.infra.service.auth.teacher.TeacherAuthTokenGranter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Component
public class SocialTokenGranter {
    private final AuthenticationManager authenticationManager;

    // DI social services
    private final GoogleAuthService googleAuthService;
    private final NaverAuthService naverAuthService;
    private final FacebookAuthService facebookAuthService;
    private final AppleAuthService appleAuthService;


    public TokenGranter socialTokenGranterList(AuthorizationServerEndpointsConfigurer endpoints) {
        List<TokenGranter> tokenGranters = new ArrayList<>();
        tokenGranters.add(
                new RefreshTokenGranter(
                        endpoints.getTokenServices(),
                        endpoints.getClientDetailsService(),
                        endpoints.getOAuth2RequestFactory()
                )
        );
        tokenGranters.add(
                new TeacherAuthTokenGranter(
                        endpoints.getTokenServices(),
                        endpoints.getClientDetailsService(),
                        endpoints.getOAuth2RequestFactory(),
                        authenticationManager,
                        DaekyoOAuth2Type.PASSWORD
                )
        );
        tokenGranters.add(
                new GoogleAuthTokenGranter(
                        endpoints.getTokenServices(),
                        endpoints.getClientDetailsService(),
                        endpoints.getOAuth2RequestFactory(),
                        authenticationManager,
                        googleAuthService,
                        DaekyoOAuth2Type.GOOGLE
                )
        );
        tokenGranters.add(
                new NaverAuthTokenGranter(
                        endpoints.getTokenServices(),
                        endpoints.getClientDetailsService(),
                        endpoints.getOAuth2RequestFactory(),
                        authenticationManager,
                        naverAuthService,
                        DaekyoOAuth2Type.NAVER
                )
        );
        tokenGranters.add(
                new FacebookAuthTokenGranter(
                        endpoints.getTokenServices(),
                        endpoints.getClientDetailsService(),
                        endpoints.getOAuth2RequestFactory(),
                        authenticationManager,
                        facebookAuthService,
                        DaekyoOAuth2Type.FACEBOOK
                )
        );
        tokenGranters.add(
                new AppleAuthTokenGranter(
                        endpoints.getTokenServices(),
                        endpoints.getClientDetailsService(),
                        endpoints.getOAuth2RequestFactory(),
                        authenticationManager,
                        appleAuthService,
                        DaekyoOAuth2Type.APPLE
                )
        );

        return new CompositeTokenGranter(tokenGranters);
    }
}
