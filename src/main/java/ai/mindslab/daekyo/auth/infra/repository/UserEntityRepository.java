package ai.mindslab.daekyo.auth.infra.repository;

import ai.mindslab.daekyo.auth.infra.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserEntityRepository extends CrudRepository<UserEntity, Long> {
    Optional<UserEntity> findBySocialAndEmail(String social, String email);
    Optional<UserEntity> findByName(String username);
}
