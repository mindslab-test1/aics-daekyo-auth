package ai.mindslab.daekyo.auth.infra.service.auth.google;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * {
 *     "issued_to": "845468432722-8nv1n79unttll8gdan081kt2kirnrc91.apps.googleusercontent.com",
 *     "audience": "845468432722-8nv1n79unttll8gdan081kt2kirnrc91.apps.googleusercontent.com",
 *     "user_id": "105641253562017796722",
 *     "scope": "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/contacts.readonly openid https://www.googleapis.com/auth/userinfo.profile",
 *     "expires_in": 271,
 *     "email": "wnstn8372@gmail.com",
 *     "verified_email": true,
 *     "access_type": "online"
 * }
 */

@Getter
@Setter
public class GoogleAccessTokenInfo {
   @JsonProperty("issued_to")
   private String issuedTo;
   @JsonProperty("audience")
   private String audience;
   @JsonProperty("user_id")
   private String userId;
   @JsonProperty("scope")
   private String scope;
   @JsonProperty("expires_in")
   private Long expiresIn;
   @JsonProperty("email")
   private String email;
   @JsonProperty("verified_email")
   private boolean verifiedEmail;
   @JsonProperty("access_type")
   private String accessType;
}
