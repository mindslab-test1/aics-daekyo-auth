package ai.mindslab.daekyo.auth.infra.service.auth.naver;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

@RequiredArgsConstructor
@Service
public class NaverAuthService {
    private final RestTemplate restTemplate;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public Pair<String, String> authorize(String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(accessToken);
        HttpEntity httpEntity = new HttpEntity(headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("https://openapi.naver.com/v1/nid/me");

        ResponseEntity<NaverResponseInfo<NaverAccountInfo>> naverAccountInfoResponseEntity = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, httpEntity, new ParameterizedTypeReference<NaverResponseInfo<NaverAccountInfo>>() {});
        if (naverAccountInfoResponseEntity.getStatusCode() != HttpStatus.OK) {
            throw new InvalidTokenException("Failed naver authentication about access_token. please check expired or invalid token.");
        }

        NaverResponseInfo<NaverAccountInfo> naverResponseInfo = naverAccountInfoResponseEntity.getBody();
        NaverAccountInfo naverAccountInfo = naverResponseInfo.getResponse();

        return new ImmutablePair<>(naverAccountInfo.getId(), naverAccountInfo.getEmail());
    }
}
