package ai.mindslab.daekyo.auth.infra.service.auth.apple;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AppleKeys {
    private List<AppleKey> keys;
}
