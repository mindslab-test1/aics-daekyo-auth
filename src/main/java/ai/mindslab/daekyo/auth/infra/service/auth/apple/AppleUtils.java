package ai.mindslab.daekyo.auth.infra.service.auth.apple;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class AppleUtils {

    /**
     * [Reference]
     * - https://developer.apple.com/documentation/sign_in_with_apple/sign_in_with_apple_rest_api/verifying_a_user
     */
    public static boolean checkIdToken(String idToken, String issueUrl, String clientId, AppleKeys appleKeys) throws ParseException {
        SignedJWT signedJWT = SignedJWT.parse(idToken);
        JWTClaimsSet payload = signedJWT.getJWTClaimsSet();

        Date currentTime = new Date(System.currentTimeMillis());
        if (!currentTime.before(payload.getExpirationTime())) {
            return false;
        }

        if (!issueUrl.equals(payload.getIssuer()) || !clientId.equals(payload.getAudience().get(0))) {
            return false;
        }

        return checkPublicKey(signedJWT, appleKeys.getKeys());
    }

    /**
     * Apple Server에서 공개 키를 받아 서명 확인하는 API
     */
    public static boolean checkPublicKey(SignedJWT signedJWT, List<AppleKey> appleKeys) {
        ObjectMapper objectMapper = new ObjectMapper();
        for (AppleKey appleKey: appleKeys) {
            try {
                RSAKey rsaKey = (RSAKey) JWK.parse(objectMapper.writeValueAsString(appleKey));
                RSAPublicKey publicKey = rsaKey.toRSAPublicKey();
                JWSVerifier verifier = new RSASSAVerifier(publicKey);

                if (signedJWT.verify(verifier)) {
                    return true;
                }
            } catch (ParseException | JsonProcessingException | JOSEException e) {
                throw new RuntimeException(e.getLocalizedMessage());
            }
        }
        return false;
    }

    /**
     * [Reference]
     * - https://developer.apple.com/documentation/sign_in_with_apple/generate_and_validate_tokens
     */
    public static String createClientSecret(String keyId, String teamId, String issueUrl, String clientId, String encodedPrivateKey) {
        return Jwts.builder()
                .setHeaderParam(JwsHeader.KEY_ID, keyId)
                .setHeaderParam("alg", SignatureAlgorithm.ES256.getValue())
                .setIssuer(teamId)
                .setAudience(issueUrl)
                .setSubject(clientId)
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 5))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .signWith(SignatureAlgorithm.ES256, encodedPrivateKey)
                .compact();
    }

    public static String createClientSecret(String keyId, String teamId, String issueUrl, String clientId, PrivateKey privateKey) {
        return Jwts.builder()
                .setHeaderParam(JwsHeader.KEY_ID, keyId)
                .setHeaderParam("alg", "ES256")
                .setIssuer(teamId)
                .setAudience(issueUrl)
                .setSubject(clientId)
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 5))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .signWith(SignatureAlgorithm.ES256, privateKey)
                .compact();
    }

    public static ApplePayload decodeFromIdToken(String idToken) throws ParseException, JsonProcessingException {
        SignedJWT signedJWT = SignedJWT.parse(idToken);
        JWTClaimsSet jwtClaimsSet = signedJWT.getJWTClaimsSet();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(jwtClaimsSet.toJSONObject().toJSONString(), ApplePayload.class);
    }
}
