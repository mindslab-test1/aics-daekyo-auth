package ai.mindslab.daekyo.auth.infra.config;

import ai.mindslab.daekyo.auth.infra.service.RestTemplateClientHttpRequestInterceptor;
import ai.mindslab.daekyo.auth.infra.service.RestTemplateErrorHandler;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class HttpConnectionConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder()
                .requestFactory(() -> new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()))
                .errorHandler(new RestTemplateErrorHandler())
                .setConnectTimeout(Duration.ofMinutes(1))
                .additionalInterceptors(new RestTemplateClientHttpRequestInterceptor())
                .build();
    }
}
