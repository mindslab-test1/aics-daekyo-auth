package ai.mindslab.daekyo.auth.infra.service.auth.apple;

import ai.mindslab.daekyo.auth.infra.config.property.social.AppleProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

@RequiredArgsConstructor
@Component
public class AppleAuthAPI {
    private final RestTemplate restTemplate;
    private final AppleProperty appleProperty;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private HttpHeaders defaultHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf("application/json;charset=UTF-8"));
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        return headers;
    }

    /**
     * 공개 키 서명 발급 API
     */
    public AppleKeys getPublicKey() {
        HttpHeaders headers = defaultHeaders();
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(appleProperty.getPublicKeyUrl());

        ResponseEntity<String> appleKeysResponseEntity = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, httpEntity, String.class);
        if (appleKeysResponseEntity.getStatusCode() != HttpStatus.OK) {
            throw new InvalidTokenException("Failed to get apple publicKey.");
        }
        String jsonAppleKeysString = new String(appleKeysResponseEntity.getBody().getBytes(), StandardCharsets.UTF_8);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            AppleKeys appleKeys = objectMapper.readValue(jsonAppleKeysString, AppleKeys.class);
            return appleKeys;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    /**
     * OAuth2 Spec의 grant_type 방식중 하나인 authorization_code 방식으로 AccessToken을 얻는 API
     */
    public AppleTokenInfo getAccessToken(String clientSecret, String authorizationCode) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, Object> requestParam = new LinkedMultiValueMap<>();
        requestParam.add("client_id", appleProperty.getClientId());
        requestParam.add("client_secret", clientSecret);
        requestParam.add("grant_type", "authorization_code");
        requestParam.add("code", authorizationCode);

        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(requestParam, headers);

        ResponseEntity<AppleTokenInfo> appleTokenInfoResponseEntity = restTemplate.exchange(appleProperty.getAuthTokenUrl(), HttpMethod.POST, httpEntity, AppleTokenInfo.class);
        if (appleTokenInfoResponseEntity.getStatusCode() != HttpStatus.OK) {
            throw new InvalidTokenException("Failed apple authentication about authorization_code. please check invalid token.");
        }
        return appleTokenInfoResponseEntity.getBody();
    }

    /**
     * OAuth2 Spec의 grant_type 방식중 하나인 refresh_token 방식으로 AccessToken 갱신하는 API
     */
    public AppleTokenInfo getRefreshToken(String clientSecret, String refreshToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, Object> requestParam = new LinkedMultiValueMap<>();
        requestParam.add("client_id", appleProperty.getClientId());
        requestParam.add("client_secret", clientSecret);
        requestParam.add("grant_type", "refresh_token");
        requestParam.add("refresh_token", refreshToken);

        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(requestParam, headers);

        ResponseEntity<AppleTokenInfo> appleTokenInfoResponseEntity = restTemplate.exchange(appleProperty.getAuthTokenUrl(), HttpMethod.POST, httpEntity, AppleTokenInfo.class);
        if (appleTokenInfoResponseEntity.getStatusCode() != HttpStatus.OK) {
            throw new InvalidTokenException("Failed apple authentication about authorization_code. please check invalid token.");
        }
        return appleTokenInfoResponseEntity.getBody();
    }
}
