package ai.mindslab.daekyo.auth.infra.service.auth.facebook;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonProperty;

@Getter
@Setter
public class FacebookMetadata {
    @JsonProperty("auth_type")
    private String authType;
    @JsonProperty("sso")
    private String sso;
}
