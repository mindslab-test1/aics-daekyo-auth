package ai.mindslab.daekyo.auth.infra.service;

import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.core.model.Role;
import ai.mindslab.daekyo.auth.core.service.DaekyoUserDetailsService;
import ai.mindslab.daekyo.auth.core.service.UdongTeacherDetailsService;
import ai.mindslab.daekyo.auth.util.AuthenticationUtils;
import ai.mindslab.daekyo.auth.util.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * RefreshToken으로 부터 AccessToken을 얻고자 할때 쓰는 Service Class
 * 통합 인증 서버라 각 도메인에 따른 DB에 접근하여 조회해야합니다.
 * 도메인 분리 기준은 authorities의 ROLE을 기준으로 분리하였습니다.
 */
@RequiredArgsConstructor
@Service
public class CustomAuthenticationUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {
    private final UdongTeacherDetailsService udongTeacherDetailsService;
    private final DaekyoUserDetailsService daekyoUserDetailsService;

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        GrantedAuthority grantedAuthority = token.getAuthorities().stream().findFirst().get();
        Role role = Role.valueOf(grantedAuthority.getAuthority().replace("ROLE_", ""));
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) token.getPrincipal();
        String username = usernamePasswordAuthenticationToken.getPrincipal().toString();
        if (Role.USER == role) {
            Map<String, Object> decodedDetails = AuthenticationUtils.getDetailsMap(token);
            if (decodedDetails.isEmpty()) {
                throw new RuntimeException("Not found details info in token. Please sign in again to get the correct refresh_token.");
            }
            DaekyoOAuth2Type daekyoOAuth2Type = DaekyoOAuth2Type.valueOf(decodedDetails.get(Constants.GRANT_TYPE).toString().toUpperCase());
            String email = decodedDetails.get(Constants.EMAIL).toString();
            username = daekyoOAuth2Type.name() + ":" + email;
        }
        switch (role) {
            case TEACHER:
                return udongTeacherDetailsService.loadUserByUsername(username);
            case USER:
                return daekyoUserDetailsService.loadUserByUsername(username);
            default:
                break;
        }
        return null;
    }
}
