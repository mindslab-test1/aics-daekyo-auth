package ai.mindslab.daekyo.auth.infra.service.auth.teacher;

import ai.mindslab.daekyo.auth.core.exception.InvalidParameterException;
import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.util.Constants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * package org.springframework.security.oauth2.provider.password;
 * reference "ResourceOwnerPasswordTokenGranter" class
 *
 */
public class TeacherAuthTokenGranter extends AbstractTokenGranter {
    private AuthenticationManager authenticationManager;
    private DaekyoOAuth2Type grantType;

    public TeacherAuthTokenGranter(
            AuthorizationServerTokenServices tokenServices,
            ClientDetailsService clientDetailsService,
            OAuth2RequestFactory requestFactory,
            AuthenticationManager authenticationManager,
            DaekyoOAuth2Type grantType
    ) {
        super(tokenServices, clientDetailsService, requestFactory, grantType.name().toLowerCase());
        this.authenticationManager = authenticationManager;
        this.grantType = grantType;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
//        Map<String, String> requestParams = tokenRequest.getRequestParameters();
//        // TODO: 선생님의 경우, 첫 로그인 시 company_no로 value를 받아도 되지 않을까?
//        String username = requestParams.get(Constants.USERNAME);
//        if (username == null) {
//            throw new RuntimeException("Not found username");
//        }
//        String password = requestParams.get("password");
//        try {
//            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
//                    username,
//                    password,
//                    client.getAuthorities()
//            );
//            usernamePasswordAuthenticationToken.setDetails(Collections.singletonMap(Constants.GRANT_TYPE, this.grantType.name().toLowerCase()));
//            Authentication userAuth = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
//            OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(tokenRequest.createOAuth2Request(client), userAuth);
//            return oAuth2Authentication;
//        } catch (Exception e) {
//            throw new BadCredentialsException("Failed TeacherAuth Grant" + e.getLocalizedMessage());
//        }
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
        String username = parameters.get(Constants.USERNAME);
        String password = parameters.get(Constants.PASSWORD);

        if (StringUtils.isBlank(password)) {
            throw new InvalidParameterException("It is a wrong password. (empty or only whitespace)");
        }

        // Protect from downstream leaks of password
        parameters.remove(Constants.PASSWORD);

        Authentication userAuth = new UsernamePasswordAuthenticationToken(username, password);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);
        try {
            userAuth = authenticationManager.authenticate(userAuth);
        } catch (AccountStatusException ase) {
            //covers expired, locked, disabled cases (mentioned in section 5.2, draft 31)
            throw new InvalidGrantException(ase.getMessage());
        } catch (BadCredentialsException e) {
            // If the username/password are wrong the spec says we should send 400/invalid grant
            throw new InvalidGrantException(e.getMessage());
        }
        if (userAuth == null || !userAuth.isAuthenticated()) {
            throw new InvalidGrantException("Could not authenticate user: " + username);
        }

        OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        return new OAuth2Authentication(storedOAuth2Request, userAuth);
    }
}
