package ai.mindslab.daekyo.auth.infra.config.property;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Getter
@Configuration
public class JwtSignKeyProperty {

    @Value("${jwt.public-key}")
    private String jwtPublicKey;

    @Value("${jwt.private-key}")
    private String jwtPrivateKey;

    @Value("${jwt.sign-key:daekyo-api-1234}")
    private String jwtSignKey;

    public KeyPair generateKeyPair() {
        return new KeyPair(getPublicKey(), getPrivateKey());
    }

    private KeyFactory getKeyFactoryWithRSA() {
        try {
            return KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private PrivateKey getPrivateKey() {
        try {
            String privateKey = jwtPrivateKey.replace("-----BEGIN PRIVATE KEY-----", "")
                                                .replace("-----END PRIVATE KEY-----", "")
                                                .replaceAll("\\s+", "");
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
            return getKeyFactoryWithRSA().generatePrivate(pkcs8EncodedKeySpec);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    private PublicKey getPublicKey() {
        try {
            String publicKey = jwtPublicKey.replace("-----BEGIN PUBLIC KEY-----", "")
                                            .replace("-----END PUBLIC KEY-----", "")
                                            .replaceAll("\\s+", "");
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
            return getKeyFactoryWithRSA().generatePublic(x509EncodedKeySpec);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }
}
