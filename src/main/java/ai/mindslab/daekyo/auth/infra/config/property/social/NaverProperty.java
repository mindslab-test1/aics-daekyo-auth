package ai.mindslab.daekyo.auth.infra.config.property.social;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class NaverProperty {
    @Value("${social.naver.client.client-id}")
    private String clientId;
    @Value("${social.naver.client.client-secret}")
    private String clientSecret;
}
