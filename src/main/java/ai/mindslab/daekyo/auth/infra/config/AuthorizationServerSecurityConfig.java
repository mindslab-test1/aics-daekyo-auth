package ai.mindslab.daekyo.auth.infra.config;

import ai.mindslab.daekyo.auth.infra.config.filter.JsonToUrlEncodedAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerSecurityConfiguration;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

@RequiredArgsConstructor
@Configuration
@Order
public class AuthorizationServerSecurityConfig extends AuthorizationServerSecurityConfiguration {

    private final JsonToUrlEncodedAuthenticationFilter jsonFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterBefore(jsonFilter, ChannelProcessingFilter.class);
        super.configure(http);
    }
}
