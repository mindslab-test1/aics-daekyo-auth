package ai.mindslab.daekyo.auth.infra.repository;

import ai.mindslab.daekyo.auth.core.model.teacher.Teacher;
import ai.mindslab.daekyo.auth.core.repository.TeacherRepository;
import ai.mindslab.daekyo.auth.infra.entity.TeacherEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RequiredArgsConstructor
@Repository
public class TeacherRepositoryImpl implements TeacherRepository {
    private final TeacherEntityRepository teacherEntityRepository;

    @Override
    public Optional<Teacher> findByUsername(String username) {
        return teacherEntityRepository.findByName(username).map(teacherEntity -> teacherEntity.toModel(teacherEntity));
    }

    @Override
    public Optional<Teacher> findByCompanyNo(Long companyNo) {
        return teacherEntityRepository.findByCompanyNo(companyNo).map(teacherEntity -> teacherEntity.toModel(teacherEntity));
    }

    @Override
    public Teacher save(Teacher teacher) {
        TeacherEntity teacherEntity = teacher.toEntity(teacher);
        return teacherEntityRepository.save(teacherEntity).toModel(teacherEntity);
    }
}
