package ai.mindslab.daekyo.auth.infra.entity;

import ai.mindslab.daekyo.auth.core.model.teacher.Teacher;
import ai.mindslab.daekyo.auth.util.ConverterUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZoneId;

@Getter
@Setter
@Entity
@Table(name = "DK_UD_TEACHER")
@AttributeOverride(name = "createdAt", column = @Column(name = "CRT_DATE", columnDefinition = "date default sysdate"))
@AttributeOverride(name = "updatedAt", column = @Column(name = "UPT_DATE", columnDefinition = "date default sysdate"))
public class TeacherEntity extends BaseEntity {
    @Id
    @SequenceGenerator(name = "TEACHER_SEQ_GEN", sequenceName = "SEQ_TEACHER_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEACHER_SEQ_GEN")
    @Column(name = "TEACHER_ID")
    private Long id;

    @Column(name = "TEACHER_COMPANY_NO", unique = true, nullable = false)
    private Long companyNo;

    @Column(name = "TEACHER_PW")
    private String password;

    @Column(name = "TEACHER_BIRTH")
    private String birth;

    @Column(name = "TEACHER_GENDER")
    private String gender;

    @Column(name = "TEACHER_NAME")
    private String name;

    @Column(name = "TEACHER_IMAGE_URL")
    private String imageUrl;

    @Column(name = "TEACHER_TEL")
    private String tel;

    @Column(name = "TEACHER_EMAIL", unique = true, nullable = false)
    private String email;

    @Column(name = "SCHEDULE_YN")
    private String schedule = "N";

    @Column(name = "REQUEST_YN")
    private String request = "N";

    @Column(name = "RESET_PW_TOKEN")
    private String resetPasswordToken;

    @Column(name = "RESET_PW_TOKEN_EXPIRED_AT")
    private Instant resetPasswordTokenExpiredAt;

    @Column(name = "TEACHER_READING_YN")
    private String reading;

    @Column(name = "TEACHER_COACHING_YN")
    private String coaching;

    @Column(name = "TEACHER_LOCKED")
    private Boolean locked = false;

    @Column(name = "TEACHER_ACTIVE")
    private Boolean active = true;

    @Column(name = "TEACHER_CONTACT")
    private String contact;

    @Column(name = "TEACHER_ADDRESS")
    private String address;

    @Column(name = "TEACHER_INTRODUCE")
    private String introduce;

    @Column(name = "TEACHER_ACTIVE_C")
    private String activeC = "N";

    @Column(name = "TEACHER_ACTIVE_T")
    private String activeT = "N";

    public Teacher toModel(TeacherEntity teacherEntity) {
        return Teacher.builder()
                .id(teacherEntity.getId())
                .companyNo((teacherEntity.getCompanyNo() == null) ? 0L : teacherEntity.getCompanyNo())
                .passwordHashed(teacherEntity.getPassword())
                .birth((StringUtils.isEmpty(teacherEntity.getBirth())) ? StringUtils.EMPTY : teacherEntity.getBirth())
                .gender((StringUtils.isEmpty(teacherEntity.getGender())) ? StringUtils.EMPTY : teacherEntity.getGender())
                .name((StringUtils.isEmpty(teacherEntity.getName())) ? StringUtils.EMPTY : teacherEntity.getName())
                .imageUrl((StringUtils.isEmpty(teacherEntity.getImageUrl())) ? StringUtils.EMPTY : teacherEntity.getImageUrl())
                .tel((StringUtils.isEmpty(teacherEntity.getTel())) ? StringUtils.EMPTY : teacherEntity.getTel())
                .email((StringUtils.isEmpty(teacherEntity.getEmail())) ? StringUtils.EMPTY : teacherEntity.getEmail())
                .schedule(ConverterUtils.convertToBoolean(teacherEntity.getSchedule()))
                .request(ConverterUtils.convertToBoolean(teacherEntity.getRequest()))
                .resetPasswordToken((StringUtils.isEmpty(teacherEntity.getResetPasswordToken())) ? StringUtils.EMPTY : teacherEntity.getResetPasswordToken())
                .resetPasswordTokenExpiredAt((teacherEntity.getResetPasswordTokenExpiredAt() == null) ? null : teacherEntity.getResetPasswordTokenExpiredAt().atZone(ZoneId.systemDefault()))
                .reading(ConverterUtils.convertToBoolean(teacherEntity.getReading()))
                .coaching(ConverterUtils.convertToBoolean(teacherEntity.getCoaching()))
                .locked(teacherEntity.getLocked())
                .active(teacherEntity.getActive())
                .createdAt((teacherEntity.getCreatedAt() == null) ? null : teacherEntity.getCreatedAt().atZone(ZoneId.systemDefault()))
                .updatedAt((teacherEntity.getUpdatedAt() == null) ? null : teacherEntity.getUpdatedAt().atZone(ZoneId.systemDefault()))
                .contact((StringUtils.isEmpty(teacherEntity.getContact())) ? StringUtils.EMPTY : teacherEntity.getContact())
                .address((StringUtils.isEmpty(teacherEntity.getAddress())) ? StringUtils.EMPTY : teacherEntity.getAddress())
                .introduce((StringUtils.isEmpty(teacherEntity.getIntroduce())) ? StringUtils.EMPTY : teacherEntity.getIntroduce())
                .activeC(ConverterUtils.convertToBoolean(teacherEntity.getActiveC()))
                .activeT(ConverterUtils.convertToBoolean(teacherEntity.getActiveT()))
                .build();
    }
}
