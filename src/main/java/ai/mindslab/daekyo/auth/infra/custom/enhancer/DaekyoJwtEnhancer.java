package ai.mindslab.daekyo.auth.infra.custom.enhancer;

import ai.mindslab.daekyo.auth.core.model.teacher.TeacherPrincipal;
import ai.mindslab.daekyo.auth.core.model.user.UserPrincipal;
import ai.mindslab.daekyo.auth.util.AuthenticationUtils;
import ai.mindslab.daekyo.auth.util.Constants;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class DaekyoJwtEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		Map<String, Object> additionalInfo = new HashMap<>();
		if (authentication.getPrincipal() instanceof TeacherPrincipal) {
			TeacherPrincipal teacherPrincipal = (TeacherPrincipal) authentication.getPrincipal();
			additionalInfo.put(Constants.COMPANY_NO, teacherPrincipal.getTeacher().getCompanyNo());
			additionalInfo.put(Constants.EMAIL, teacherPrincipal.getTeacher().getEmail());
			((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
			return accessToken;
		} else if (authentication.getPrincipal() instanceof UserPrincipal) {
			UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
			Map<String, Object> details = AuthenticationUtils.getDetailsMap(authentication);
			additionalInfo.put(Constants.SOCIAL_ID, userPrincipal.getUser().getSocialId());
			additionalInfo.put(Constants.EMAIL, userPrincipal.getUser().getEmail());
			additionalInfo.put(Constants.GRANT_TYPE,
					(!details.isEmpty())? details.get(Constants.GRANT_TYPE).toString()
							: AuthenticationUtils.getDetailsMap(authentication.getUserAuthentication()).get(Constants.GRANT_TYPE).toString()
			);
			((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
			return accessToken;
		} else {
			return accessToken;
		}
	}
}
