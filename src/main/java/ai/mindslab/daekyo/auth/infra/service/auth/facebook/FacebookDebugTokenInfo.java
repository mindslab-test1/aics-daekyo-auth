package ai.mindslab.daekyo.auth.infra.service.auth.facebook;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@Getter
@Setter
public class FacebookDebugTokenInfo {
    @JsonProperty("app_id")
    private String appId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("application")
    private String application;
    @JsonProperty("data_access_expires_at")
    private Long dataAccessExpiresAt;
    @JsonProperty("expires_at")
    private Long expiresAt;
    @JsonProperty("is_valid")
    private boolean isValid;
    @JsonProperty("issued_at")
    private Long issuedAt;
    @JsonProperty("metadata")
    private FacebookMetadata facebookMetadata;
    @JsonProperty("scopes")
    private List<String> scopes;
    @JsonProperty("user_id")
    private String userId;
}
