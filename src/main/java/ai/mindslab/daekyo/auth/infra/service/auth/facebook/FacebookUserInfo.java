package ai.mindslab.daekyo.auth.infra.service.auth.facebook;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FacebookUserInfo {
    private String id;
    private String name;
    private String email;
}
