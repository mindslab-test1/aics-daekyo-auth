package ai.mindslab.daekyo.auth.infra.config.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Getter
@Setter
@Configuration
@ConfigurationProperties("oauth2")
public class OAuth2ClientProperty {

    private List<OAuth2Client> clients;

    @Getter
    @Setter
    public static class OAuth2Client {
        private String clientId;
        private String clientSecret;
        private String authority;
        private List<String> grantTypes;
        private Integer accessTokenExpires = 3600 * 24 * 14; // 2 weeks (second)
        private Integer refreshTokenExpires = 3600 * 24 * 90; // 90 days (second)
    }
}
