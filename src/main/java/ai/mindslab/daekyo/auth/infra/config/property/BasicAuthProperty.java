package ai.mindslab.daekyo.auth.infra.config.property;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class BasicAuthProperty {

    @Value("${basic-auth.admin.username}")
    private String adminUsername;

    @Value("${basic-auth.admin.password}")
    private String adminPassword;

    @Value("${basic-auth.admin.authority}")
    private String adminAuthority;
}
