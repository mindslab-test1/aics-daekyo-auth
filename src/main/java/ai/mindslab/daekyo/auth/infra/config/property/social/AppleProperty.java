package ai.mindslab.daekyo.auth.infra.config.property.social;

import lombok.Getter;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileReader;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

@Getter
@Configuration
public class AppleProperty {
    @Value("${social.apple.client.client-id}")
    private String clientId;
    @Value("${social.apple.client.issue-url")
    private String issueUrl;
    @Value("${social.apple.client.team-id}")
    private String teamId;
    @Value("${social.apple.client.key-id}")
    private String keyId;
    @Value("${social.apple.auth.token-url}")
    private String authTokenUrl;
    @Value("${social.apple.auth.public-key-url}")
    private String publicKeyUrl;
    @Value("${social.apple.auth.private-key}")
    private String privateKey;

    public PrivateKey generatePrivateKey() {
        try {
            String privateKey = getPrivateKey().replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "")
                    .replaceAll("\\s+", "");
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
            return getKeyFactory("EC").generatePrivate(pkcs8EncodedKeySpec);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    public String getEncodedPrivateKeyString() {
        return getPrivateKey().replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "")
                .replaceAll("\\s+", "");
    }

    public PrivateKey generatePrivateKeyWithResourceFile() {
        //read your key
        Resource resource = new ClassPathResource("apple/AuthKey_UWMUHSZK72.p8");
        try (PEMParser pemParser = new PEMParser(new FileReader(resource.getURI().getPath()))) {
            final JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
            final PrivateKeyInfo object = (PrivateKeyInfo) pemParser.readObject();
            final PrivateKey pKey = converter.getPrivateKey(object);
            return pKey;
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    private KeyFactory getKeyFactory(String alg) {
        try {
            return KeyFactory.getInstance(alg);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
