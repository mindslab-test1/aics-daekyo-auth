package ai.mindslab.daekyo.auth.infra.entity;

import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.core.model.user.User;
import ai.mindslab.daekyo.auth.util.ConverterUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZoneId;

@Getter
@Setter
@Entity
@Table(name = "DK_USER")
@AttributeOverride(name = "createdAt", column = @Column(name = "CRT_DATE", columnDefinition = "date default sysdate"))
@AttributeOverride(name = "updatedAt", column = @Column(name = "UPD_DATE", columnDefinition = "date default sysdate"))
public class UserEntity extends BaseEntity {
    @Id
    @SequenceGenerator(name = "USER_SEQ_GEN", sequenceName = "SEQ_USER_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ_GEN")
    @Column(name = "USER_ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SOCIAL", nullable = false)
    private String social;

    @Column(name = "SOCIAL_ID", nullable = false)
    private String socialId;

    @Column(name = "SIGN_UP_DATE", nullable = false)
    private Instant signUpAt;

    @Column(name = "SIGN_OUT_DATE")
    private Instant signOutAt;

    @Column(name = "SERIAL_NO")
    private String serialNo;

    @Column(name = "USER_PERMISSION", nullable = false)
    private String userPermission = "N";

    public User toModel(UserEntity userEntity) {
        return User.builder()
                .id(userEntity.getId())
                .email((StringUtils.isEmpty(userEntity.getEmail())) ? StringUtils.EMPTY : userEntity.getEmail())
                .name((StringUtils.isEmpty(userEntity.getName())) ? StringUtils.EMPTY : userEntity.getName())
                .social((StringUtils.isEmpty(userEntity.getSocial())) ? null : DaekyoOAuth2Type.valueOf(userEntity.getSocial().toUpperCase()))
                .socialId((StringUtils.isEmpty(userEntity.getSocialId())) ? StringUtils.EMPTY : userEntity.getSocialId())
                .signUpAt((userEntity.getSignUpAt() == null) ? null : userEntity.getSignUpAt().atZone(ZoneId.systemDefault()))
                .signOutAt((userEntity.getSignOutAt() == null) ? null : userEntity.getSignOutAt().atZone(ZoneId.systemDefault()))
                .serialNo((StringUtils.isEmpty(userEntity.getSerialNo())) ? StringUtils.EMPTY : userEntity.getSerialNo())
                .userPermission(ConverterUtils.convertToBoolean(userEntity.getUserPermission()))
                .createdAt((userEntity.getCreatedAt() == null) ? null : userEntity.getCreatedAt().atZone(ZoneId.systemDefault()))
                .updatedAt((userEntity.getUpdatedAt() == null) ? null : userEntity.getUpdatedAt().atZone(ZoneId.systemDefault()))
                .build();
    }
}
