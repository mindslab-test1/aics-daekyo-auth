package ai.mindslab.daekyo.auth.infra.repository;

import ai.mindslab.daekyo.auth.core.exception.NotFoundClientException;
import ai.mindslab.daekyo.auth.core.model.Client;
import ai.mindslab.daekyo.auth.core.repository.ClientRepository;
import ai.mindslab.daekyo.auth.infra.entity.ClientEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Primary
@RequiredArgsConstructor
@Repository
public class ClientRepositoryImpl implements ClientRepository, ClientDetailsService {
    private final ClientEntityRepository clientEntityRepository;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        Client client = findByClientId(clientId).orElseThrow(() -> new NotFoundClientException("Not found clientId(" + clientId + ")"));
        return new BaseClientDetails(client);
    }

    @Override
    public Client save(Client client) {
        ClientEntity clientEntity = client.toEntity(client);
        return clientEntityRepository.save(clientEntity).toModel(clientEntity);
    }

    @Override
    public Optional<Client> findById(Long id) {
        return clientEntityRepository.findById(id).map(clientEntity -> clientEntity.toModel(clientEntity));
    }

    @Override
    public Optional<Client> findByClientId(String clientId) {
        return clientEntityRepository.findByClientId(clientId).map(clientEntity -> clientEntity.toModel(clientEntity));
    }

    @Override
    public Page<Client> findAll(Pageable pageable) {
        return clientEntityRepository.findAll(pageable).map(clientEntity -> clientEntity.toModel(clientEntity));
    }
}
