package ai.mindslab.daekyo.auth.infra.config;

import ai.mindslab.daekyo.auth.infra.config.property.JwtSignKeyProperty;
import ai.mindslab.daekyo.auth.infra.custom.converter.CustomAccessTokenConverter;
import ai.mindslab.daekyo.auth.infra.custom.enhancer.DaekyoJwtEnhancer;
import ai.mindslab.daekyo.auth.infra.custom.granter.SocialTokenGranter;
import ai.mindslab.daekyo.auth.infra.service.CustomAuthenticationUserDetailsService;
import ai.mindslab.daekyo.auth.infra.service.CustomTokenServices;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;

import java.util.Arrays;

@RequiredArgsConstructor
@EnableAuthorizationServer
@Configuration
public class OAuth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private final JwtSignKeyProperty jwtSignKeyProperty;
    private final ClientDetailsService clientDetailsService;

    private final SocialTokenGranter socialTokenGranter;
    private final AuthenticationManager authenticationManager;
    private final CustomAuthenticationUserDetailsService customUserDetailsByNameServiceWrapper;
    private final CustomAccessTokenConverter customAccessTokenConverter;

    private final WebResponseExceptionTranslator<OAuth2Exception> customWebResponseExceptionTranslator;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(clientDetailsService);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(daekyoJwtEnhancer(), jwtAccessTokenConverter()));

        endpoints.tokenGranter(socialTokenGranter.socialTokenGranterList(endpoints));

        endpoints
                .tokenServices(customTokenServices())
                .tokenEnhancer(tokenEnhancerChain)
                .authenticationManager(authenticationManager)
                .exceptionTranslator(customWebResponseExceptionTranslator);
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setAccessTokenConverter(customAccessTokenConverter);
        converter.setKeyPair(jwtSignKeyProperty.generateKeyPair());
        return converter;
    }

    @Bean
    @Primary
    public DefaultTokenServices customTokenServices() throws Exception {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(daekyoJwtEnhancer(), jwtAccessTokenConverter()));

        DefaultTokenServices defaultTokenServices = new CustomTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        defaultTokenServices.setTokenEnhancer(tokenEnhancerChain);
        defaultTokenServices.setAuthenticationManager(new ProviderManager(preAuthenticatedAuthenticationProvider()));
        // clientDetails가 inMemory방식이 아닌 DB Access 방식으로 진행하는 경우, 아래처럼 주입해야 한다.
        // ref: https://github.com/spring-projects/spring-security-oauth/issues/1448#issuecomment-482025681
        defaultTokenServices.setClientDetailsService(clientDetailsService);
        return defaultTokenServices;
    }

    @Bean
    @Primary
    public PreAuthenticatedAuthenticationProvider preAuthenticatedAuthenticationProvider() {
        PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
        provider.setPreAuthenticatedUserDetailsService(customUserDetailsByNameServiceWrapper);
        provider.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return provider;
    }

    @Description("Register enhancer for edit jwt claim")
    @Bean
    public TokenEnhancer daekyoJwtEnhancer() {
        return new DaekyoJwtEnhancer();
    }
}
