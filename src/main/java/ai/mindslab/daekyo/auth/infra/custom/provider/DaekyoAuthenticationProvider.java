package ai.mindslab.daekyo.auth.infra.custom.provider;

import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.core.service.UdongTeacherDetailsService;
import ai.mindslab.daekyo.auth.core.usecase.teacher.CheckUdongTeacherUsecase;
import ai.mindslab.daekyo.auth.core.usecase.user.RegisterUserUsecase;
import ai.mindslab.daekyo.auth.util.AuthenticationUtils;
import ai.mindslab.daekyo.auth.util.Constants;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Map;

@RequiredArgsConstructor
@Primary
@Component
public class DaekyoAuthenticationProvider implements AuthenticationProvider {
    private final RegisterUserUsecase registerUserUsecase;
    private final CheckUdongTeacherUsecase checkUdongTeacherUsecase;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    // 여기가 소셜 로그인 및 회원가입 플레이그라운드 영역임.
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String uniqueId = (String) authentication.getPrincipal();
        String clientId = authentication.getName();

        DaekyoOAuth2Type daekyoOAuth2Type;
        String email = "";
        String name = "";
        String password = "";

        Map<String, Object> decodedDetails = AuthenticationUtils.getDetailsMap(authentication);
        if (decodedDetails.isEmpty()) {
            throw new RuntimeException("Not found grant type about '" + clientId +"'");
        }

        daekyoOAuth2Type = DaekyoOAuth2Type.valueOf(decodedDetails.get(Constants.GRANT_TYPE).toString().toUpperCase());
        if (DaekyoOAuth2Type.PASSWORD != daekyoOAuth2Type) {
            email = decodedDetails.get(Constants.EMAIL).toString();
            name = decodedDetails.getOrDefault(Constants.NAME, "").toString();
        } else {
            password = authentication.getCredentials().toString();
        }

        UserDetails userDetails;

        switch (daekyoOAuth2Type) {
            case GOOGLE:
            case FACEBOOK:
            case KAKAO:
            case NAVER:
            case APPLE:
                userDetails = registerUserUsecase.execute(daekyoOAuth2Type, uniqueId, email, name);
                break;
            default: // case Udong Teacher
                // 회원가입이 따로 없어 조회 및 검증만 함.
                userDetails = checkUdongTeacherUsecase.execute(uniqueId, password);
                break;
        }

        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
