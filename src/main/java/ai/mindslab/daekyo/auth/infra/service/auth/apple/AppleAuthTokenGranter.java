package ai.mindslab.daekyo.auth.infra.service.auth.apple;

import ai.mindslab.daekyo.auth.core.exception.InvalidParameterException;
import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.util.Constants;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AppleAuthTokenGranter extends AbstractTokenGranter {
    private AuthenticationManager authenticationManager;
    private AppleAuthService appleAuthService;
    private DaekyoOAuth2Type grantType;

    public AppleAuthTokenGranter(
            AuthorizationServerTokenServices tokenServices,
            ClientDetailsService clientDetailsService,
            OAuth2RequestFactory requestFactory,
            AuthenticationManager authenticationManager,
            AppleAuthService appleAuthService,
            DaekyoOAuth2Type grantType
    ) {
        super(tokenServices, clientDetailsService, requestFactory, grantType.name().toLowerCase());
        this.authenticationManager = authenticationManager;
        this.appleAuthService = appleAuthService;
        this.grantType = grantType;
    }

    public OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> requestParams = tokenRequest.getRequestParameters();
        String idToken = requestParams.get(Constants.ID_TOKEN);
        String authorizationCode = requestParams.get(Constants.CODE);
        boolean isVerify = Boolean.parseBoolean(requestParams.getOrDefault(Constants.IS_VERIFY, "true"));

        String apple = this.grantType.name().toLowerCase();

        if (idToken == null) {
            throw new InvalidParameterException("Required id_token");
        }

        if (authorizationCode == null && isVerify) {
            throw new InvalidParameterException("Required code");
        }

        Pair<String, String> pair = appleAuthService.authorize(idToken, (isVerify) ? authorizationCode : "test", isVerify);
        String socialId = pair.getLeft();
        String email = pair.getRight();
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(socialId, null, client.getAuthorities());
        Map<String, Object> details = new HashMap<>();
        details.put(Constants.GRANT_TYPE, apple);
        details.put(Constants.EMAIL, email);
        usernamePasswordAuthenticationToken.setDetails(details);

        // 이미 가입된 유저인지 신규 유저인지 체크하기 위해 DaekyoAuthenticationProvider에게 확인 및 등록 요청
        Authentication userAuth = authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(tokenRequest.createOAuth2Request(client), userAuth);
        oAuth2Authentication.setDetails(Collections.singletonMap(Constants.GRANT_TYPE, apple));
        oAuth2Authentication.setAuthenticated(true);
        return oAuth2Authentication;
    }
}
