package ai.mindslab.daekyo.auth.infra.custom.converter;

import ai.mindslab.daekyo.auth.util.Constants;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CustomAccessTokenConverter extends DefaultAccessTokenConverter {

    @Override
    public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
        OAuth2Authentication oAuth2Authentication = super.extractAuthentication(map);
        Map<String, Object> decodedDetails = new HashMap<>();
        if (map.containsKey(Constants.SOCIAL_ID)) {
            String social_id = (String) map.get(Constants.SOCIAL_ID);
            decodedDetails.put(Constants.SOCIAL_ID.toLowerCase(), social_id);
        }
        if (map.containsKey(Constants.GRANT_TYPE)) {
            String grant_type = (String) map.get(Constants.GRANT_TYPE);
            decodedDetails.put(Constants.GRANT_TYPE.toLowerCase(), grant_type);
        }
        if (map.containsKey(Constants.EMAIL)) {
            String email = (String) map.get(Constants.EMAIL);
            decodedDetails.put(Constants.EMAIL.toLowerCase(), email);
        }

        oAuth2Authentication.setDetails(decodedDetails);
        return oAuth2Authentication;
    }
}
