package ai.mindslab.daekyo.auth.infra.config.property;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class DaekyoProperty {

    @Value("${daekyo.api.app-key}")
    private String appKey;

    @Value("${daekyo.api.url}")
    private String serverUrl;
}
