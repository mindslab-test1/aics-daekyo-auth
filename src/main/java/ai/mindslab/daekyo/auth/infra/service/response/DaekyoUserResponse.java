package ai.mindslab.daekyo.auth.infra.service.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class DaekyoUserResponse {
    private long userId;
    private String email;
    private String name;
    private String social;
    private String socialId;
    private String signUpDate;
    private String signOutDate;
    private String serialNo;
    private String realName;
    private String phoneNo;

    private String accessToken;
    private long expireTime;
    private long seqNo;
    private String requestContent;

    // 앱 이용권 구매 여부
    private String voucherYn;
    private String mainChildExistYn;
    private int childCount;
    private String mainChildName;
    private String mainChildNickName;
    private String childProfileImage;
    private String voucherId;

    // 시리얼번호 등록여부
    private String serialRegYn;

    private Date crtDate;
    private Date updDate;
}
