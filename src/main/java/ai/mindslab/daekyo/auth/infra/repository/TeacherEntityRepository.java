package ai.mindslab.daekyo.auth.infra.repository;

import ai.mindslab.daekyo.auth.infra.entity.TeacherEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TeacherEntityRepository extends CrudRepository<TeacherEntity, Long> {
    Optional<TeacherEntity> findByName(String username);
    Optional<TeacherEntity> findByCompanyNo(Long companyNo);
}
