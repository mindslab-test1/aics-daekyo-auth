package ai.mindslab.daekyo.auth.infra.service.auth.facebook;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FacebookResponseInfo<T> {
    private T data;
}
