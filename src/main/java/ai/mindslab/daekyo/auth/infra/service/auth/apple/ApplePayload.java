package ai.mindslab.daekyo.auth.infra.service.auth.apple;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplePayload {
    @JsonProperty("iss")
    private String iss;
    @JsonProperty("aud")
    private String aud;
    @JsonProperty("exp")
    private Long exp;
    @JsonProperty("iat")
    private Long iat;
    @JsonProperty("sub") // ClientId
    private String sub;
    @JsonProperty("c_hash")
    private String cHash;
    @JsonProperty("email")
    private String email;
    @JsonProperty("email_verified")
    private String emailVerified;
    @JsonProperty("auth_time")
    private Long authTime;
    @JsonProperty("nonce_supported")
    private boolean nonceSupported;
}
