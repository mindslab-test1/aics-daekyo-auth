package ai.mindslab.daekyo.auth.infra.service.auth.apple;

import ai.mindslab.daekyo.auth.infra.config.property.social.AppleProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@RequiredArgsConstructor
@Service
public class AppleAuthService {
    private final AppleAuthAPI appleAuthAPI;
    private final AppleProperty appleProperty;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public Pair<String, String> authorize(String idToken, String authorizationCode, boolean isVerify) {
        // Is not verify id-token
        if (!authorizationCode.equalsIgnoreCase("test") && isVerify) {
            // validate idToken
            try {
                if (!AppleUtils.checkIdToken(
                        idToken,
                        appleProperty.getIssueUrl(),
                        appleProperty.getClientId(),
                        appleAuthAPI.getPublicKey())
                ) {
                    throw new InvalidTokenException("Failed apple authentication about idToken. please check invalid token.");
                }
            } catch (ParseException e) {
                logger.error(e.getLocalizedMessage());
                throw new InvalidTokenException(e.getLocalizedMessage());
            }

            String clientSecret = AppleUtils.createClientSecret(
                    appleProperty.getKeyId(),
                    appleProperty.getTeamId(),
                    appleProperty.getIssueUrl(),
                    appleProperty.getClientId(),
                    appleProperty.generatePrivateKeyWithResourceFile());

            // validate authorization_code
            AppleTokenInfo appleTokenInfo = appleAuthAPI.getAccessToken(clientSecret, authorizationCode);
            if (!idToken.equals(appleTokenInfo.getIdToken())) {
                throw new InvalidTokenException("Failed apple authentication about idToken. the requested idToken and apple's idToken do not match.");
            }
        }
        ApplePayload applePayload;
        try {
            applePayload = AppleUtils.decodeFromIdToken(idToken);
        } catch (ParseException | JsonProcessingException e) {
            logger.error(e.getLocalizedMessage());
            throw new InvalidTokenException(e.getLocalizedMessage());
        }
        String email = applePayload.getEmail();
        String clientId = applePayload.getSub();
        return new ImmutablePair<>(clientId, email);
    }
}
