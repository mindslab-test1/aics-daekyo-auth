package ai.mindslab.daekyo.auth.infra.service.auth.facebook;

import ai.mindslab.daekyo.auth.infra.config.property.social.FacebookProperty;
import ai.mindslab.daekyo.auth.util.Constants;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

@RequiredArgsConstructor
@Component
public class FacebookGraphAPI {
    private final RestTemplate restTemplate;
    private final FacebookProperty facebookProperty;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private HttpHeaders defaultHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

    /**
     * [Reference]
     * - https://developers.facebook.com/docs/graph-api/reference/v9.0/debug_token
     * - https://developers.facebook.com/docs/facebook-login/access-tokens/#apptokens
     * - https://fortune94.tistory.com/347
     */
    public FacebookDebugTokenInfo debugToken(String inputToken) {
        final String appAccessToken = facebookProperty.getClientId() + "|" + facebookProperty.getClientSecret();

        HttpHeaders headers = defaultHeaders();
        HttpEntity<FacebookResponseInfo<FacebookDebugTokenInfo>> httpEntity = new HttpEntity<>(headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("https://graph.facebook.com/debug_token")
                // Add query parameter
                .queryParam(Constants.INPUT_TOKEN, inputToken)
                .queryParam(Constants.ACCESS_TOKEN, appAccessToken);

        ResponseEntity<FacebookResponseInfo<FacebookDebugTokenInfo>> responseInfoResponseEntity = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, httpEntity, new ParameterizedTypeReference<FacebookResponseInfo<FacebookDebugTokenInfo>>() {});
        FacebookResponseInfo<FacebookDebugTokenInfo> facebookResponseInfo = responseInfoResponseEntity.getBody();
        if (responseInfoResponseEntity.getStatusCode() != HttpStatus.OK) {
            throw new InvalidTokenException("Failed facebook authentication about access_token. please check expired or invalid token.");
        }
        return facebookResponseInfo.getData();
    }

    /**
     * [Reference]
     * - https://developers.facebook.com/docs/graph-api/using-graph-api/common-scenarios#------------------------
     */
    public FacebookUserInfo me(String accessToken) {
        HttpHeaders headers = defaultHeaders();
        headers.setBearerAuth(accessToken);
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("https://graph.facebook.com/v3.3/me")
                // Add query parameter
                .queryParam(Constants.FIELDS, "id,name,email");

        ResponseEntity<FacebookUserInfo> facebookUserInfoResponseEntity = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, httpEntity, FacebookUserInfo.class);
        if (facebookUserInfoResponseEntity.getStatusCode() != HttpStatus.OK) {
            throw new InvalidTokenException("Failed facebook authentication about access_token. please check expired or invalid token.");
        }

        return facebookUserInfoResponseEntity.getBody();
    }
}
