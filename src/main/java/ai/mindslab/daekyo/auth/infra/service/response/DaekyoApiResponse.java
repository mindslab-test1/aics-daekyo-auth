package ai.mindslab.daekyo.auth.infra.service.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DaekyoApiResponse {
    private int code;
    private String msg;
    private DaekyoUserResponse data;
}
