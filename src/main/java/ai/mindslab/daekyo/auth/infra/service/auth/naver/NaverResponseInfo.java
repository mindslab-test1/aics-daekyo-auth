package ai.mindslab.daekyo.auth.infra.service.auth.naver;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NaverResponseInfo<T> {
    private String responseCode;
    private String message;
    private T response;
}
