package ai.mindslab.daekyo.auth.infra.repository;

import ai.mindslab.daekyo.auth.infra.entity.ClientEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ClientEntityRepository extends PagingAndSortingRepository<ClientEntity, Long> {
    Optional<ClientEntity> findByClientId(String clientId);
}
