package ai.mindslab.daekyo.auth.infra.service.auth.google;

import ai.mindslab.daekyo.auth.util.Constants;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

@RequiredArgsConstructor
@Service
public class GoogleAuthService {
    private final RestTemplate restTemplate;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public Pair<String, String> authorize(String accessToken) {
        // TODO: Verify idToken
        // Replace to verify access-token
        // why? google library not working...
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity httpEntity = new HttpEntity(headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("https://www.googleapis.com/oauth2/v1/tokeninfo")
                // Add query parameter
                .queryParam(Constants.ACCESS_TOKEN, accessToken);

        ResponseEntity<GoogleAccessTokenInfo> googleAccessTokenInfoResponseEntity = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, httpEntity, GoogleAccessTokenInfo.class);
        if (googleAccessTokenInfoResponseEntity.getStatusCode() != HttpStatus.OK) {
            throw new InvalidTokenException("Failed google authentication about access_token. please check expired or invalid token.");
        }

        GoogleAccessTokenInfo googleAccessTokenInfo = googleAccessTokenInfoResponseEntity.getBody();

        return new ImmutablePair<>(googleAccessTokenInfo.getUserId(), googleAccessTokenInfo.getEmail());
    }
}
