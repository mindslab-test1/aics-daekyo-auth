package ai.mindslab.daekyo.auth.core.exception;

public class NotAllowedMethodException extends CustomOAuth2Exception{
    public NotAllowedMethodException(String msg, Throwable t) {
        super(msg, t);
    }
    @Override
    public String getOAuth2ErrorCode() {
        return "method_not_allowed";
    }

    @Override
    public int getHttpErrorCode() {
        return 405;
    }
}
