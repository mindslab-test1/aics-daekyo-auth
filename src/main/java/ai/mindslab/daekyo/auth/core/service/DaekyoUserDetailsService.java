package ai.mindslab.daekyo.auth.core.service;

import ai.mindslab.daekyo.auth.core.exception.NotFoundUserException;
import ai.mindslab.daekyo.auth.core.model.user.User;
import ai.mindslab.daekyo.auth.core.model.user.UserPrincipal;
import ai.mindslab.daekyo.auth.core.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class DaekyoUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    private Logger logger = LoggerFactory.getLogger(DaekyoUserDetailsService.class);

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user;
        if (username.contains(":")) { // case to use refresh_token
            String[] params = username.split(":");
            String social = params[0];
            String email = params[1];
            user = userRepository.findBySocialAndEmail(social, email).orElseThrow(() -> new NotFoundUserException("Not found email/social(" + email + ", " + social + ")"));
            if (user.getName() == null) {
                user.setName(user.getSocialId());
            }
        } else { // case to get access_token
            user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Not found username(" + username + ")"));
        }
        UserPrincipal userPrincipal = new UserPrincipal(user);
        return userPrincipal;
    }
}
