package ai.mindslab.daekyo.auth.core.exception;

import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

@Getter
public class NotFoundTeacherException extends AuthenticationException {

    public NotFoundTeacherException(String msg) {
        super(msg);
    }
}
