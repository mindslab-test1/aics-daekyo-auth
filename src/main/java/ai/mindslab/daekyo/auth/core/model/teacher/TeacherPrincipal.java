package ai.mindslab.daekyo.auth.core.model.teacher;


import ai.mindslab.daekyo.auth.core.model.Role;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;


@RequiredArgsConstructor
@Getter
public class TeacherPrincipal implements UserDetails {
    private final Teacher teacher;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(new SimpleGrantedAuthority(Role.TEACHER.getKey()));
    }

    @Override
    public String getPassword() {
        return teacher.getPasswordHashed();
    }

    @Override
    public String getUsername() {
        return teacher.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !teacher.isLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return teacher.isActive();
    }
}
