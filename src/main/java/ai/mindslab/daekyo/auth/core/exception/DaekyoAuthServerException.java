package ai.mindslab.daekyo.auth.core.exception;

public class DaekyoAuthServerException extends CustomOAuth2Exception {
    public DaekyoAuthServerException(String msg, Throwable t) {
        super(msg, t);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "server_error";
    }

    @Override
    public int getHttpErrorCode() {
        return 500;
    }
}
