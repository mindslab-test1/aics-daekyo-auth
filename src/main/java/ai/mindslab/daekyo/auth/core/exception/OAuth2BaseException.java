package ai.mindslab.daekyo.auth.core.exception;

import lombok.Getter;

@Getter
public class OAuth2BaseException extends CustomOAuth2Exception {
    private final String code;
    private final String message;

    public OAuth2BaseException(String msg, Throwable t, String code, String message) {
        super(msg, t);
        this.code = code;
        this.message = message;
    }
}
