package ai.mindslab.daekyo.auth.core.repository;

import ai.mindslab.daekyo.auth.core.model.teacher.Teacher;

import java.util.Optional;

public interface TeacherRepository {
    Optional<Teacher> findByUsername(String username);
    Optional<Teacher> findByCompanyNo(Long companyNo);
    Teacher save(Teacher teacher);
}
