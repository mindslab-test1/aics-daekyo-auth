package ai.mindslab.daekyo.auth.core.usecase.client;

import ai.mindslab.daekyo.auth.core.exception.NotFoundClientException;
import ai.mindslab.daekyo.auth.core.model.Client;
import ai.mindslab.daekyo.auth.core.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Component
public class ClientDetailUsecase {
    private final ClientRepository clientRepository;

    @Transactional
    public Client execute(Long id) {
        return clientRepository.findById(id).orElseThrow(() -> new NotFoundClientException("Not found client about id(" + id + ")"));
    }
}
