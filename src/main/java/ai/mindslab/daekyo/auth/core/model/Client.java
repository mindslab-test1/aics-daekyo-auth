package ai.mindslab.daekyo.auth.core.model;

import ai.mindslab.daekyo.auth.infra.entity.ClientEntity;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.util.CollectionUtils;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Client implements ClientDetails {
    private Long id;
    private String clientId;
    private String clientSecret;
    private Collection<String> resourceIds;

    // ClientDetails Interface에 getScope가 선언되어져 있으므로,
    // @Data로 인해 생성되는 scope 접근레벨을 None으로 변경
    @Getter(AccessLevel.NONE)
    private Collection<String> scopes;

    private Collection<String> authorizedGrantTypes;
    private Collection<String> webServerRedirectUris;
    private Collection<String> authorities;
    private Integer accessTokenValiditySeconds;
    private Integer refreshTokenValiditySeconds;
    private Map<String, Object> additionalInformation;
    private boolean autoApprove;
    private ZonedDateTime createdAt;
    private ZonedDateTime updatedAt;

    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        return new HashSet<>(resourceIds);
    }

    @Override
    public boolean isSecretRequired() {
        return !clientSecret.isEmpty();
    }

    @Override
    public String getClientSecret() {
        return clientSecret;
    }

    @Override
    public boolean isScoped() {
        return !scopes.isEmpty();
    }

    @Override
    public Set<String> getScope() {
        return new HashSet<>(scopes);
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return new HashSet<>(authorizedGrantTypes);
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return new HashSet<>(webServerRedirectUris);
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities.stream().map(DaekyoAuthority::fromValue).collect(Collectors.toSet());
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return accessTokenValiditySeconds;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return refreshTokenValiditySeconds;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return autoApprove;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return additionalInformation;
    }

    public ClientEntity toEntity(Client client) {
        ClientEntity entity = new ClientEntity();
        entity.setId(client.getId());
        entity.setClientId(client.getClientId());
        entity.setClientSecret(client.getClientSecret());
        entity.setResourceIds((CollectionUtils.isEmpty(client.getResourceIds())) ? StringUtils.EMPTY : String.join(",", client.getResourceIds()));
        entity.setScopes((CollectionUtils.isEmpty(client.getScope())) ? StringUtils.EMPTY : String.join(",", client.getScope()));
        entity.setAuthorizedGrantTypes((CollectionUtils.isEmpty(client.getAuthorizedGrantTypes())) ? StringUtils.EMPTY : String.join(",", client.getAuthorizedGrantTypes()));
        entity.setWebServerRedirectUris((CollectionUtils.isEmpty(client.getWebServerRedirectUris())) ? StringUtils.EMPTY : String.join(",", client.getWebServerRedirectUris()));
        entity.setAuthorities((CollectionUtils.isEmpty(client.getAuthorities())) ? StringUtils.EMPTY : String.join(",", client.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet())));
        entity.setAccessTokenValiditySeconds(client.getAccessTokenValiditySeconds());
        entity.setRefreshTokenValiditySeconds(client.getRefreshTokenValiditySeconds());
        entity.setAdditionalInformation(
                (CollectionUtils.isEmpty(client.getAdditionalInformation())) ? StringUtils.EMPTY :
                        client.getAdditionalInformation().keySet().stream().map(key -> key + "=" + client.getAdditionalInformation().get(key))
                .collect(Collectors.joining(", ", "{", "}")));
        entity.setAutoApprove(client.isAutoApprove());
        entity.setCreatedAt((client.getCreatedAt() == null) ? null : client.getCreatedAt().toInstant());
        entity.setUpdatedAt((client.getUpdatedAt() == null) ? null : client.getUpdatedAt().toInstant());
        return entity;
    }
}
