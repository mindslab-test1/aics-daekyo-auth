package ai.mindslab.daekyo.auth.core.exception;

public class UnauthorizedException extends CustomOAuth2Exception {
    public UnauthorizedException(String msg, Throwable t) {
        super(msg, t);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "unauthorized";
    }

    @Override
    public int getHttpErrorCode() {
        return 401;
    }
}
