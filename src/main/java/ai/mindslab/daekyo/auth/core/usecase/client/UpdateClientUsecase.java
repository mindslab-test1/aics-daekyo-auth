package ai.mindslab.daekyo.auth.core.usecase.client;

import ai.mindslab.daekyo.auth.boundaries.dto.ClientTokenUpdateDto;
import ai.mindslab.daekyo.auth.boundaries.dto.ClientUpdateDto;
import ai.mindslab.daekyo.auth.core.exception.NotFoundClientException;
import ai.mindslab.daekyo.auth.core.model.Client;
import ai.mindslab.daekyo.auth.core.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Component
public class UpdateClientUsecase {
    private final ClientRepository clientRepository;

    @Transactional
    public Client execute(Long id, ClientUpdateDto clientUpdateDto) {
        Client client = clientRepository.findById(id).orElseThrow(() -> new NotFoundClientException("Not found client about id(" + id + ")"));
        client.setClientId(clientUpdateDto.getClientId());
        client.setClientSecret(clientUpdateDto.getClientSecret());
        client.setResourceIds(clientUpdateDto.getResourceIds());
        client.setScopes(clientUpdateDto.getScopes());
        client.setAuthorizedGrantTypes(clientUpdateDto.getAuthorizedGrantTypes());
        client.setWebServerRedirectUris(clientUpdateDto.getWebServerRedirectUris());
        client.setAuthorities(clientUpdateDto.getAuthorities());
        client.setAccessTokenValiditySeconds(clientUpdateDto.getAccessTokenValiditySeconds());
        client.setRefreshTokenValiditySeconds(clientUpdateDto.getRefreshTokenValiditySeconds());
        client.setAdditionalInformation(clientUpdateDto.getAdditionalInformation());
        client.setAutoApprove(clientUpdateDto.isAutoApprove());

        return clientRepository.save(client);
    }

    @Transactional
    public Client execute(Long id, ClientTokenUpdateDto clientTokenUpdateDto) {
        Client client = clientRepository.findById(id).orElseThrow(() -> new NotFoundClientException("Not found client about id(" + id + ")"));
        client.setAccessTokenValiditySeconds(clientTokenUpdateDto.getAccessTokenValiditySeconds());
        client.setRefreshTokenValiditySeconds(clientTokenUpdateDto.getRefreshTokenValiditySeconds());
        return clientRepository.save(client);
    }
}
