package ai.mindslab.daekyo.auth.core.usecase.teacher;

import ai.mindslab.daekyo.auth.core.exception.InvalidPasswordException;
import ai.mindslab.daekyo.auth.core.service.UdongTeacherDetailsService;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

//@RequiredArgsConstructor
@Component
public class CheckUdongTeacherUsecase {
    private UdongTeacherDetailsService udongTeacherDetailsService;
    private PasswordEncoder passwordEncoder;

    // BeanCurrentlyInCreationException: The dependencies of some of the beans in the application context form a cycle
    // Bean 순환 참조 문제로 PasswordEncoder만 Lazy로 반영
    public CheckUdongTeacherUsecase(UdongTeacherDetailsService udongTeacherDetailsService, @Lazy PasswordEncoder passwordEncoder) {
        this.udongTeacherDetailsService = udongTeacherDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDetails execute(String companyNo, String password) {
        UserDetails userDetails = udongTeacherDetailsService.loadUserByUsername(companyNo);
        if (!passwordEncoder.matches(password, userDetails.getPassword())) {
            throw new InvalidPasswordException("Invalid password for companyNo(" + companyNo + ")");
        }
        return userDetails;
    }
}
