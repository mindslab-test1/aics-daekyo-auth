package ai.mindslab.daekyo.auth.core.model;

public enum DaekyoOAuth2Type {
    GOOGLE,
    FACEBOOK,
    KAKAO,
    NAVER,
    APPLE,
    PASSWORD;
}
