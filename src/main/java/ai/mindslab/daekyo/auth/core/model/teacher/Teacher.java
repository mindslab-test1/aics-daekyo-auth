package ai.mindslab.daekyo.auth.core.model.teacher;

import ai.mindslab.daekyo.auth.infra.entity.TeacherEntity;
import ai.mindslab.daekyo.auth.util.ConverterUtils;
import lombok.*;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    // primary key
    private Long id;

    // unique (This will be used as the user login id)
    private Long companyNo;
    private String passwordHashed;

    private String birth;
    private String gender;
    private String name;
    private String imageUrl;
    private String tel;
    // unique
    private String email;

    private boolean schedule;
    private boolean request;

    // Reset Password Token
    private String resetPasswordToken;
    private ZonedDateTime resetPasswordTokenExpiredAt;

    private boolean reading;
    private boolean coaching;

    private boolean locked;
    private boolean active;

    // ISO-8601
    private ZonedDateTime createdAt;
    private ZonedDateTime updatedAt;

    private String contact;
    private String address;
    private String introduce;
    private boolean activeC;
    private boolean activeT;

    public TeacherEntity toEntity(Teacher teacher) {
        TeacherEntity entity = new TeacherEntity();
        entity.setId(teacher.getId());
        entity.setCompanyNo(teacher.getCompanyNo());
        entity.setPassword(teacher.getPasswordHashed());
        entity.setBirth(teacher.getBirth());
        entity.setGender(teacher.getGender());
        entity.setName(teacher.getName());
        entity.setImageUrl(teacher.getImageUrl());
        entity.setTel(teacher.getTel());
        entity.setEmail(teacher.getEmail());
        entity.setSchedule(ConverterUtils.convertToString(teacher.isSchedule()));
        entity.setRequest(ConverterUtils.convertToString(teacher.isRequest()));
        entity.setResetPasswordToken(teacher.getResetPasswordToken());
        entity.setResetPasswordTokenExpiredAt((teacher.getResetPasswordTokenExpiredAt() == null)? null : teacher.getResetPasswordTokenExpiredAt().toInstant());
        entity.setReading(ConverterUtils.convertToString(teacher.isReading()));
        entity.setCoaching(ConverterUtils.convertToString(teacher.isCoaching()));
        entity.setLocked(teacher.isLocked());
        entity.setActive(teacher.isActive());
        entity.setCreatedAt((teacher.getCreatedAt() == null)? null : teacher.getCreatedAt().toInstant());
        entity.setUpdatedAt((teacher.getUpdatedAt() == null)? null : teacher.getUpdatedAt().toInstant());
        entity.setContact(teacher.getContact());
        entity.setAddress(teacher.getAddress());
        entity.setIntroduce(teacher.getIntroduce());
        entity.setActiveC(ConverterUtils.convertToString(teacher.isActiveC()));
        entity.setActiveT(ConverterUtils.convertToString(teacher.isActiveT()));
        return entity;
    }
}
