package ai.mindslab.daekyo.auth.core.exception;

import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

@Getter
public class InvalidPasswordException extends AuthenticationException {

    public InvalidPasswordException(String msg) {
        super(msg);
    }
}
