package ai.mindslab.daekyo.auth.core.exception;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class DaekyoAuthApiResponseError {
    private String code;
    private String message;
}
