package ai.mindslab.daekyo.auth.core.exception;

public class ForbiddenException extends CustomOAuth2Exception {
    public ForbiddenException(String msg, Throwable t) {
        super(msg, t);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "access_denied";
    }

    @Override
    public int getHttpErrorCode() {
        return 403;
    }
}
