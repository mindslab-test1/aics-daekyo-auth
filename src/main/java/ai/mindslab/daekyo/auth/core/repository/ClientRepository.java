package ai.mindslab.daekyo.auth.core.repository;

import ai.mindslab.daekyo.auth.core.model.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ClientRepository {
    Client save(Client client);
    Optional<Client> findById(Long id);
    Optional<Client> findByClientId(String clientId);
    Page<Client> findAll(Pageable pageable);
}
