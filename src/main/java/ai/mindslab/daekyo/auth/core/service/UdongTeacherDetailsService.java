package ai.mindslab.daekyo.auth.core.service;

import ai.mindslab.daekyo.auth.core.exception.NotFoundTeacherException;
import ai.mindslab.daekyo.auth.core.model.teacher.Teacher;
import ai.mindslab.daekyo.auth.core.model.teacher.TeacherPrincipal;
import ai.mindslab.daekyo.auth.core.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UdongTeacherDetailsService implements UserDetailsService {
    private final TeacherRepository teacherRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Teacher teacher;
        try {
            // process when user login
            teacher = teacherRepository.findByCompanyNo(Long.parseLong(username)).orElseThrow(() -> new NotFoundTeacherException("Not found companyNo(" + username + ")"));
        } catch (NumberFormatException e) {
            // process when renewing token through refresh-token
            teacher = teacherRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Not found username(" + username + ")"));
        }
        TeacherPrincipal teacherPrincipal = new TeacherPrincipal(teacher);
        return teacherPrincipal;
    }
}
