package ai.mindslab.daekyo.auth.core.exception;

import lombok.Getter;

@Getter
public class NotFoundClientException extends BaseException {
    private final String message;

    public NotFoundClientException(String message) {
        super("not_found_client", message);
        this.message = message;
    }
}
