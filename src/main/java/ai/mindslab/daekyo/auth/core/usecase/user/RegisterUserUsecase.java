package ai.mindslab.daekyo.auth.core.usecase.user;

import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.core.model.user.User;
import ai.mindslab.daekyo.auth.core.model.user.UserPrincipal;
import ai.mindslab.daekyo.auth.core.repository.UserRepository;
import ai.mindslab.daekyo.auth.infra.config.property.DaekyoProperty;
import ai.mindslab.daekyo.auth.infra.service.response.DaekyoApiResponse;
import ai.mindslab.daekyo.auth.infra.service.response.DaekyoUserResponse;
import ai.mindslab.daekyo.auth.util.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class RegisterUserUsecase {
    private final UserRepository userRepository;
    private final RestTemplate restTemplate;
    private final DaekyoProperty daekyoProperty;

    public UserPrincipal execute(DaekyoOAuth2Type daekyoOAuth2Type, String uniqueId, String email, String name) {
        User user = userRepository.findBySocialAndEmail(daekyoOAuth2Type.name(), email).orElse(null);
        // 이미 존재하는 유저의 경우 바로 return
        if (user != null) {
            if (user.getName() == null) {
                user.setName(uniqueId);
            }
            return new UserPrincipal(user);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add(Constants.APP_KEY_HEADER, daekyoProperty.getAppKey());

        Map<String, Object> map = new HashMap<>();
        map.put("social", daekyoOAuth2Type.name().toUpperCase());
        map.put("socialId", uniqueId);
        map.put("email", email);

        // optional field
        if (!name.isEmpty()) {
            map.put("nickName", name);
        }

        HttpEntity<Map<String, Object>> httpEntity = new HttpEntity<>(map, headers);

        ResponseEntity<DaekyoApiResponse> responseEntity = restTemplate.postForEntity(daekyoProperty.getServerUrl(), httpEntity, DaekyoApiResponse.class);
        DaekyoUserResponse daekyoUserResponse = responseEntity.getBody().getData();

        User newUser = User.builder()
                .email(daekyoUserResponse.getEmail())
                .socialId(daekyoUserResponse.getSocialId())
                .build();

        if ((daekyoUserResponse.getName() == null) && (daekyoUserResponse.getRealName() == null)) {
            newUser.setName(daekyoUserResponse.getSocialId());
        } else if (daekyoUserResponse.getRealName() != null) {
            newUser.setName(daekyoUserResponse.getRealName());
        } else {
            newUser.setName(daekyoUserResponse.getName());
        }

        return new UserPrincipal(newUser);
    }

}
