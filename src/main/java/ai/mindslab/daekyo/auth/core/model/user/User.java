package ai.mindslab.daekyo.auth.core.model.user;

import ai.mindslab.daekyo.auth.core.model.DaekyoOAuth2Type;
import ai.mindslab.daekyo.auth.infra.entity.UserEntity;
import ai.mindslab.daekyo.auth.util.ConverterUtils;
import lombok.*;

import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private long id;
    private String email;
    private String name;
    private DaekyoOAuth2Type social;
    private String socialId;
    private ZonedDateTime signUpAt;
    private ZonedDateTime signOutAt;
    private String serialNo;
    private boolean userPermission;
    private ZonedDateTime createdAt;
    private ZonedDateTime updatedAt;

    public UserEntity toEntity(User user) {
        UserEntity entity = new UserEntity();
        entity.setId(user.getId());
        entity.setEmail(user.getEmail());
        entity.setName(user.getName());
        entity.setSocial(user.getSocial().name().toUpperCase());
        entity.setSocialId(user.getSocialId());
        entity.setSignUpAt(user.getSignUpAt().toInstant());
        entity.setSignOutAt(user.getSignOutAt().toInstant());
        entity.setSerialNo(user.getSerialNo());
        entity.setUserPermission(ConverterUtils.convertToString(user.isUserPermission()));
        entity.setCreatedAt(user.getCreatedAt().toInstant());
        entity.setUpdatedAt(user.getUpdatedAt().toInstant());
        return entity;
    }
}
