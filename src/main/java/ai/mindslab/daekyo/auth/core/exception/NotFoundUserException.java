package ai.mindslab.daekyo.auth.core.exception;

import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

@Getter
public class NotFoundUserException extends AuthenticationException {

    public NotFoundUserException(String msg) {
        super(msg);
    }
}
