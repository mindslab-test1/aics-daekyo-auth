package ai.mindslab.daekyo.auth.core.exception;

public class InvalidParameterException extends CustomOAuth2Exception {

    public InvalidParameterException(String msg) {
        this(msg, null);
    }

    public InvalidParameterException(String msg, Throwable t) {
        super(msg, t);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "invalid_parameter";
    }

    @Override
    public int getHttpErrorCode() {
        return 400;
    }
}
