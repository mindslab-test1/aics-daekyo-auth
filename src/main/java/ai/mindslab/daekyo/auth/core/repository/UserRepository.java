package ai.mindslab.daekyo.auth.core.repository;

import ai.mindslab.daekyo.auth.core.model.user.User;

import java.util.Optional;

public interface UserRepository {
    Optional<User> findBySocialAndEmail(String social, String email);
    Optional<User> findByUsername(String username);
}
