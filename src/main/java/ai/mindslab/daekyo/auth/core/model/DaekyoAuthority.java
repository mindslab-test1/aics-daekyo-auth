package ai.mindslab.daekyo.auth.core.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.security.core.GrantedAuthority;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public enum DaekyoAuthority implements GrantedAuthority {
    DAEKYO_ACCOUNT("daekyo_account"),
    DAEKYO_UDONG_TEACHER("daekyo_udong_teacher"),
    DAEKYO_ADMIN("daekyo_admin");

    private static final Map<String, DaekyoAuthority> DAEKYO_AUTHORITY_MAP = Stream.of(values()).collect(toMap(Objects::toString, e -> e));

    private String value;

    DaekyoAuthority(String value) {
        this.value = value;
    }

    @JsonValue
    @Override
    public String getAuthority() {
        return this.value;
    }

    @JsonCreator
    public static DaekyoAuthority fromValue(String value) {
        return DAEKYO_AUTHORITY_MAP.getOrDefault(value, DaekyoAuthority.DAEKYO_ACCOUNT);
    }
}
